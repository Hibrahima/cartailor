# CarTailor

A Java project aiming to simulate the configuration of a car using Observer, Factory, Visitor and Command design patterns.

Make sure to have at least maven 3.1 installed before running the Junit tests. Junit 5 is used in this project which requires at least Java 8.

To run test cases, first do the following
-Clone the project
-git checkout tags/v1.0 for version 1 or git checkout v1, or git git checkout tags/v2.0 for version 2
-Run maven tests (see below)


Run test cases - classes
-Run CompatibilityManagerTest : mvn -Dtest=CompatibilityManagerTest test
-Run ConfigurationTest	      : mvn -Dtest=ConfigurationTest test
-Run ConfiguratorTest         : mvn -Dtest=ConfiguratorTest test
-Run V2ConfigurationTest      : mvn -Dtest=V2ConfigurationTest test

Run all tests                 : mvn test

To browse the Javadoc, open in a browser the index.html file in the Javadoc folder (path = /documentation/Javadoc/index.html)
