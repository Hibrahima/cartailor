package com.helper;

public enum Colors {
	
	WHITE("white"),
	BLACK("black"),
	RED("red"),
	GREEN("green"),
	YELLOW("yellow"),
	PURPLE("purple"),
	BLUE("blue"),
	BROWN("brown"),
	ORANGE("orange"),
	MAGENTA("magenta");
	
	private String color;
	private Colors(String color) {
		this.color = color;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	

}
