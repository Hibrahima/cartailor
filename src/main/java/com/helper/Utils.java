package com.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;


public class Utils {
	
	/**
	* Converts a given iterator of type T to a list of same type 
	*
	* @param <T> the generic type T to be given
	* @param data the iterator of type T to convert to list
	* @throws NullPointerException if data is null
	* @return the converted iterator as list
	*/
	public static <T> List<T> convertIteratorToList(Iterator<T> data){
		Objects.requireNonNull(data, "iterator to be converted as list cannot be null");
		List<T> toReturn = new ArrayList<>();
		data.forEachRemaining(toReturn::add);
		return toReturn;
	}
	
	/**
	* Converts a given collection of type T to an iterator of same type 
	*
	* @param <T> the generic type T to be given
	* @param data the collection of type T to convert to an iterator of same type
	* @throws NullPointerException if data is null
	* @return the converted collection as iterator
	*/
	public static <T>Iterator<T>  getIterator(Collection<T> data) {
		Objects.requireNonNull(data, "collection to be converted as iterator cannot be null");
		return data.iterator();
	}
	

}
