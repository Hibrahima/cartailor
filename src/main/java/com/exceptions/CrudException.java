package com.exceptions;

public class CrudException extends Exception {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Displays a particular message when this is exception is thrown  t
	 *
	 * @param message: the exception message to be shown (for information purpose) 
	 */
	public CrudException(String message) {
		System.out.println(message);
	}

}
