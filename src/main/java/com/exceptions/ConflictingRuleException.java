package com.exceptions;

public class ConflictingRuleException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Displays a particular message when this is exception is thrown
	 * 
	 * @param message: the exception message to be shown (for information purpose)
	 */
	public ConflictingRuleException(String message) {
		System.out.println(message);
	}

}
