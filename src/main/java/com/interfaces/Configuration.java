package com.interfaces;

import java.util.Iterator;

public interface Configuration extends CompatibilityChecker, Observable{
	
	/**
	 * Checks that whether or not a configuration is complete
	 * A configuration is complete iff it is valid and contains all 4 categories
	 * 
	 * @return a boolean that indicates the completeness of the configuration 
	 */
	public boolean isComplete();
	
	/**
	 * Checks that whether or not a configuration is valid
	 * A configuration is valid iff does not contain any incompatible parts of the parts present in the configuration
	 * and iff it contains all required parts of each part present in the configuration
	 * 
	 * @return a boolean that indicates the validness of the configuration 
	 */
	public boolean isValid();
	
	/**
	 * Adds a part to a configuration
	 * A configuration should not contain the same part more than once
	 * Displays the validness and the completeness of the configuration 
	 * Notifies all registered observers
	 * 
	 * @param part the part to add to the configuration
	 * @throws NullPointerException if part is null 
	 * @throws IllegalArgumentException if the configuration already contains part 
	 */
	public void addPart(Part part);
	
	/**
	 * Removes a part from a configuration
	 * A configuration should not contain the same part more than once
	 * Displays the validness and the completeness of the configuration 
	 * Notifies all registered observers
	 * 
	 * @param part the part to remove from the configuration
	 * @throws NullPointerException if part is null 
	 * @throws IllegalArgumentException if the configuration does not contains part 
	 */
	public void removePart(Part part);
	
	/**
	 * Retrieves the list of parts that have been added to a configuration
	 * A configuration should not contain the same part more than once
	 * 
	 * @return an iterator on the parts that are present in the configuration 
	 */
	public Iterator<Part> getParts();
	

	/**
	 * Retrieves the list of observers (configurators) that are watching (registered) the configuration
	 * A configuration should not contain the same part more than once
	 * 
	 * @return an iterator on the observers that are watching the configuration 
	 */
	public Iterator<Observer> getRegisteredObservers();
	
	
	/**
	 * Computes the total price by summing the price of each part present in the configuration
	 * Even though the configuration contains some parts, it may return 0 (zero) if the configuration is invalid
	 * 
	 * @return the calculated price
	 */
	public double getPrice();

}
