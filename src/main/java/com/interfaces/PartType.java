package com.interfaces;

import java.util.Iterator;

import com.exceptions.ConflictingRuleException;
import com.exceptions.CrudException;

public interface PartType{
	
	/**
	 * Gets the list of parts that are associated to a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the part type that is attached to a given part
	 */
	public Iterator<Part> getParts();
	
	/**
	 * Gets the name of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the name a given part type
	 */
	public String getName();
	
	/**
	 * Sets the  name of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @param name the name to set
	 * @throws NullPointerException if name is null
	 */
	public void setName(String name);
	
	/**
	 * Gets the description of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the description  of a given part type
	 */
	public String getDescription();
	
	/**
	 * Sets the  description of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @param description the description to set
	 * @throws NullPointerException if description is null
	 */
	public void setDescription(String description);
	
	/**
     * Adds a set of part types to [this] as incompatible with [this]
     *
     * @param incompatibilities the iterator that contains the incompatible part types to add to [this]
     * @throws NullPointerException if incompatibilities is null
     * @throws ConflictingRuleException if the requirement list of [this] contains an element present in the incompatibilities iterator
     * @throws ConflictingRuleException if the requirement list of an element present in the incompatibilities iterator contains [this]
     * @throws CrudException if the incompatibility list of [this] already contains an element present in the incompatibilities iterator
     * @throws CrudException if an element present in the incompatibilities iterator is equals to [this]
     */
	public void addIncompatibilities(Iterator<PartType> incompatibilities) throws ConflictingRuleException, CrudException;
	
	/**
     * Adds a set of part types to [this] as required by [this]
     *
     * @param requirements the iterator that contains the required part types to add to [this]
     * @throws NullPointerException if requirements is null
     * @throws ConflictingRuleException if the incompatibility list of [this] contains an element present in the requirements iterator
     * @throws ConflictingRuleException if the incompatibility list of an element present in the requirements iterator contains [this]
     * @throws CrudException if the requirement list of [this] already contains an element present in the requirements iterator
     * @throws CrudException if an element present in the requirements iterator is equals to [this]
     */
	public void addRequirements(Iterator<PartType> requirements) throws ConflictingRuleException, CrudException;
	
	/**
	* Removes a set of incompatible part types from [this]
	*
	* @param incompatibilities the iterator that contains the incompatible part types to remove from [this]
	* @throws NullPointerException if incompatibilities is null
	* @throws CrudException if the incompatibility list of [this] does not contain the current element present in the incompatibilities iterator
	*/
	public void removeIncompatibilities(Iterator<PartType> incompatibilities) throws CrudException;
	
	/**
	* Removes a set of required part types from [this]
	*
	* @param requirements the iterator that contains the required part types to remove from [this]
	* @throws NullPointerException if requirements is null
	* @throws CrudException if the requirement list of [this] does not contain the current element present in the requirements iterator
	*/
	public void removeRequirements(Iterator<PartType> requirements)throws CrudException;
	
	/**
	 * Retrieves the list of part types that are required by a given part type
	 *
	 * @return an iterator on the retrieved required parts
	 */
	public Iterator<PartType> getRequiredParts();
	
	/**
	 * Retrieves the incompatible part types of a given part type
	 *
	 * @return an iterator on the retrieved incompatible parts
	 */
	public Iterator<PartType> getIncompatibleParts();
	
	/**
	 * Adds a part to a given part type
	 * A part type should not contain the same part more than once
	 * 
	 * @param p the part to add to a given part type
	 * @throws NullPointerException if p is null 
	 * @throws CrudException if the part type already contains p 
	 */
	public void addPart(Part p) throws CrudException;
	
	/**
	 * Removes a part from a given part type
	 * A part type should not contain the same part more than once
	 * 
	 * @param p the part to remove from a given part type
	 * @throws NullPointerException if p is null 
	 * @throws CrudException if the part type does not contains p 
	 */
	public void removePart(Part p) throws CrudException;
	
	/**
	 * Gets the category of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * A category may have multiple part types
	 * 
	 * @return the category  of a given part type
	 */
	public Category getCategory();

}
