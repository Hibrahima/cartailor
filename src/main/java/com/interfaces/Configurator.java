package com.interfaces;



public interface Configurator extends Observer {

	/**
	 * Gets the current configuration
	 * @return the current configuration
	 */
	public Configuration getConfiguration();
}
