package com.interfaces;

/**
 * 
 * @author ibrahima
 * 
 *	Represents the element interface of the Visitor design pattern
 */
public interface Element {

	/**
	 * Calls the accept method of each part in the configuration, so that it calls
	 * at its turn the visit method of a concrete visitor
	 * Prints the html description of the configuration at the end of the process
	 * Represents the accept operation of the Visitor design pattern
	 * 
	 * @param v the visitor interface - the concrete visitor is ElementVisitor which visits a concrete part (PartImpl)
	 * @see com.impl.ElementVisitor#visit(com.impl.PartImpl)
	 */
	public void accept(Visitor v);

}
