package com.interfaces;


public interface Observable{
	
	/**
	 * Registers an observer
	 * A configuration should not register the same observer more tan once 
	 * 
	 * @param observer the observer to register
	 * @throws NullPointerException if observer is null
	 * @throws IllegalArgumentException if observer is already registered
	 */
	public void register(Observer observer) throws IllegalArgumentException;
	
	/**
	 * Unregisters an observer
	 * A configuration should not register the same observer more tan once 
	 * 
	 * @param observer the observer to unregister
	 * @throws NullPointerException if observer is null
	 * @throws IllegalArgumentException if observer is not registered
	 */
	public void unregister(Observer observer) throws IllegalArgumentException;
	
	/**
	 * Checks that whether or not a given observer is registered 
	 * A configuration should not register the same observer more tan once 
	 * 
	 * @param observer the observer to test
	 * @throws NullPointerException if observer is null
	 * @return a boolean indicating whether ot not observer is registered 
	 */
	public boolean isRegistered(Observer observer);

}
