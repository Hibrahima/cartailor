package com.interfaces;

import com.impl.PartImpl;
/**
 * 
 * @author ibrahima
 * 
 * Represents the visitor interface of the Visitor design pattern
 *
 */
public interface Visitor {
	
	/**
	 * Visits a concrete part (PartImpl)
	 * Prints the basic info about parts
	 * Represents the visit operation of the Visitor design pattern
	 * 
	 * @param p the concrete part to describe
	 */
	public void visit(PartImpl p);

}
