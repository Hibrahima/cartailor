package com.interfaces;

import java.util.Optional;
import java.util.Set;

public interface PropertyManager {
	

	/**
	 * Returns an immutable set of property names supported by the property manager
	 * 
	 * @return that immutable set
	 */
	public Set<String> getPropertyNames();
	
	/**
	 * Returns the immutable set of discrete string values for a given property
	 * Returns an empty set of for properties that have a non explicit set of possible values
	 * Returns an empty set for non existing property name
	 * 
	 * @param name a non-null string reference
	 * @return an immutable set
	 */
	public Set<String> getAvailablePropertyValues(String name);
	
	/**
	 * Return the optional value of a property
	 * If the object does not support the supplied property,an empty optional is returned
	 * 
	 * @param name the property name to read
	 * @return the optional of that property
	 */
	public Optional<String> getProperty(String name);
	
	/**
	 * Sets the value of a given property
	 *
	 * @param name the property name to set
	 * @param value the value of the property to set
	 * @throws IllegalArgumentException  if there is no such property, or if it's not writable, or if the value is invalid
	 * @throws IllegalArgumentException if value is not present in the possible values list supported by this property
	 */
	public void setProperty(String name, String value);

}
