package com.interfaces;

import java.util.Iterator;

import com.exceptions.ConflictingRuleException;
import com.exceptions.CrudException;

public interface CompatibilityManager extends CompatibilityChecker{
	
	/**
     * Adds a set of part types to reference as incompatible with that reference
     *
     * @param reference the part type whose incompatibility list is set
     * @param incompatibilities the iterator that contains the incompatible part types to add to reference
     * @throws NullPointerException if reference is null
     * @throws NullPointerException if incompatibilities is null
     * @throws ConflictingRuleException if the requirement list of reference contains an element present in the incompatibilities iterator
     * @throws ConflictingRuleException if the requirement list of an element present in the incompatibilities iterator contains reference
     * @throws CrudException if the incompatibility list of reference already contains an element present in the incompatibilities iterator
     * @throws CrudException if an element present in the incompatibilities iterator is equals to reference
     * @see com.impl.PartTypeImpl#addIncompatibilities(Iterator)
     */
	public void addIncompatibilities(PartType reference, Iterator<PartType> incompatibilities) throws ConflictingRuleException, CrudException;
	
	/**
     * Adds a set of part types to reference as required by reference
     *
     * @param reference the part type whose requirement list is set
     * @param requirements the iterator that contains the required part types to add to reference
     * @throws NullPointerException if reference is null
     * @throws NullPointerException if requirements is null
     * @throws ConflictingRuleException if the incompatibility list of reference contains an element present in the requirements iterator
     * @throws ConflictingRuleException if the incompatibility list of an element present in the requirements iterator contains reference
     * @throws CrudException if the requirement list of reference already contains an element present in the requirements iterator
     * @throws CrudException if an element present in the requirements iterator is equals to reference
     * @see com.impl.PartTypeImpl#addRequirements(Iterator)
     */
	public void addRequirements(PartType reference, Iterator<PartType> requirements) throws ConflictingRuleException, CrudException;
	
	/**
    * Removes a set of incompatible part types from reference
    *
    * @param reference the part type whose incompatibility list is updated
    * @param incompatibilities the iterator that contains the incompatible part types to remove from reference
    * @throws NullPointerException if reference is null
    * @throws NullPointerException if incompatibilities is null
    * @throws CrudException if the incompatibility list of reference does not contain the current element present in the incompatibilities iterator
    * @see com.impl.PartTypeImpl#removeIncompatibilities(Iterator)
    */
	public void removeIncompatibilities(PartType reference, Iterator<PartType> incompatibilities) throws CrudException;
	
	/**
	* Removes a set of required part types from reference
	*
	* @param reference the part type whose requirement list is updated
	* @param requirements the iterator that contains the required part types to remove from reference
	* @throws NullPointerException if reference is null
	* @throws NullPointerException if requirements is null
	* @throws CrudException if the requirement list of reference does not contain the current element present in the requirements iterator
    * @see com.impl.PartTypeImpl#removeRequirements(Iterator)
	*/
	public void removeRequirements(PartType reference, Iterator<PartType> requirements) throws CrudException;
	

}
