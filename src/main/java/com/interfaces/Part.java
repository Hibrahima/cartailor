package com.interfaces;



public interface Part extends PropertyManager, Element{
	
	/**
	 * Gets the name of a given part 
	 * 
	 * @return the name of a given part
	 */
	public String getName();
	
	/**
	 * Sets the name of a given part 
	 * 
	 * @param name the name to set
	 * @throws NullPointerException if name is null
	 */
	public void setName(String name);
	
	/**
	 * Gets the part type which is associated to a given part
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the part type that is attached to a given part
	 */
	public PartType getPartType();
	

	/**
	 * Sets the part type which is associated to a given part
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @param p the part type to associate to a given part
	 * @throws NullPointerException if p is null
	 */
	public void setPartType(PartType p);
	
}
