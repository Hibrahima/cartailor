package com.interfaces;

import java.util.Iterator;

public interface CompatibilityChecker {

	/**
	 * Retrieves the incompatible part types of a given part type
	 *
	 * @param partType the part type whose incompatible parts are retrieved
	 * @return an iterator on the retrieved incompatible parts
	 * @throws NullPointerException if partType is null
	 */
	public Iterator<PartType> getIncompatibleParts(PartType partType);

	/**
	 * Retrieves the list of part types that are required by a given part type
	 *
	 * @param partType the part type whose required parts are retrieved
	 * @return an iterator on the retrieved required parts
	 * @throws NullPointerException if partType is null
	 */
	public Iterator<PartType> getRequiredParts(PartType partType);

}
