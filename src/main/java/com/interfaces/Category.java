package com.interfaces;

import java.util.Iterator;

public interface Category {

	/**
	 * Retrieves the name of a given category
	 *
	 * @return the name of that given category
	 */
	public String getName();

	/**
	 * Retrieves the list of part types that are associated to a given category
	 *
	 * @return an iterator on retrieved part types
	 */
	public Iterator<PartType> getPartTypes();

	/**
	 * Retrieves the list of parts that are associated to a given part type that at
	 * its turn is associated to a given category
	 *
	 * @return an iterator on the retrieved parts
	 */
	public Iterator<Part> getParts();

	/**
	 * add a particular part type to a given category
	 *
	 * @param p the part type to add
	 * @throws NullPointerException if p is null
	 */
	public void addPartType(PartType p);
}
