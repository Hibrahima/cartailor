package com.interfaces;

public interface Observer {
	
	/**
	 * Updates observer so that it receives the current configuration
	 * Updates the related observer with the submitted observable object
	 * 
	 * @param observable the observable object to use to update the related observer
	 */
	public void update(Observable observable);

}
