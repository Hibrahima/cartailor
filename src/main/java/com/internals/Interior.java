package com.internals;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import com.impl.PartImpl;

public abstract class Interior extends PartImpl{
	
	private String type = "standard";
	private Set<String> possibleTypes;
	
	public Interior() {
		possibleTypes = new HashSet<>();
		possibleTypes.add("standard");
		possibleTypes.add("finish");
		possibleTypes.add("high-end");
		
		addProperty("type", () -> getType(), (c) -> setType(c), possibleTypes);
	}
	
	//@Override
	public String getType() {
		return this.type;
	}
	
	private void setType(String type) {
		Objects.requireNonNull(type, "type cannot be null");
		this.type = type;
	}
	
	@Override
	public double getPrice() {
		switch (this.type) {
		case "standard":
			return 1000d;
		case "finish":
			return 2875d;
		case "high-end":
			return 3200d;
		default:
			return 4;
		}
	}


}
