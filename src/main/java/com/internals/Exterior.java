package com.internals;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.helper.Colors;
import com.impl.PartImpl;

public abstract class Exterior extends PartImpl {
	
	private Colors color = Colors.WHITE;
	private Set<String> possibleColors;
	
	public Exterior() {
		possibleColors= new HashSet<>();
		possibleColors.add(Colors.RED.getColor());
		possibleColors.add(Colors.MAGENTA.getColor());
		possibleColors.add(Colors.PURPLE.getColor());
		
		addProperty("color", () -> getColor(), (c) -> setColor(c), possibleColors);
	}
	
	
	@Override
	public String getColor() {
		return this.color.getColor();
	}
	
	private void setColor(String color) {
		Objects.requireNonNull(color, "color cannot be null");
		this.color.setColor(color);
	}
	
	
	public String getDescription() {
		switch (this.color.getColor()) {
		case "red":
			return "Very expensive exterior part type but it worths it";
		case "magenta":
			return "xc Magenta is one of the lovely exterior part types, especially for women";
		case "white":
			return "Just wonderful part";
		case "purple":
			return "this part is less expensive but is compliant with lots of engines";

		default:
			return "no description";
		}
	}

	
	@Override
	public double getPrice() {
		switch (this.color.getColor()) {
		case "red":
			return 4000d;
		case "magenta":
			return 2700d;
		case "white":
			return 3200d;
		case "purple":
			return 1299d;

		default:
			return 0;
		}
	}

}
