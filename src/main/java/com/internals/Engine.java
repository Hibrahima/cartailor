package com.internals;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import com.impl.PartImpl;

public abstract class Engine extends PartImpl {

	private double power = 100;
	private String category = "gasoline";
	private Set<String> possiblePowers;
	private Set<String> possibleCategories;

	public Engine() {
		possiblePowers = new HashSet<>();
		possiblePowers.add("100");
		possiblePowers.add("133");
		possiblePowers.add("210");
		possiblePowers.add("133");
		possiblePowers.add("110");
		possiblePowers.add("180");
		possiblePowers.add("120");
		
		possibleCategories = new HashSet<>();
		possibleCategories.add("gasoline");
		possibleCategories.add("diesel");
		
		addProperty("category", () -> getCategory(), (c) -> setCategory(c), possibleCategories);
		addProperty("power", () -> getPower(), (c) -> setPower(c), possiblePowers);
	}
	
	public void setCategory(String category) {
		Objects.requireNonNull(category, "category cannot be null");
		this.category = category;
	}
	public String getCategory() {
		return this.category;
	}

	public String getPower() {
		return String.valueOf(this.power);
	}

	private void setPower(String power) {
		Objects.requireNonNull(power, "power cannot be null");
		this.power = Double.valueOf(power);
	}

	@Override
	public double getPrice() {
		switch (String.valueOf(this.power)) {
		case "100.0":
			return this.category == "gasoline" ? 499 : 399;
		case "133.0":
			return this.category == "gasoline" ? 599 : 499;
		case "210.0":
			return this.category == "gasoline" ? 699 : 599;
		case "110.0":
			return this.category == "gasoline" ? 599 : 429;
		case "180.0":
			return this.category == "gasoline" ? 699 : 529;
		case "120.0":
			return this.category == "gasoline" ? 559 : 459;
		default:
			return 0;
		}
	}

}
