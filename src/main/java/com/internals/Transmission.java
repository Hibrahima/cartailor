package com.internals;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.impl.PartImpl;

public class Transmission extends PartImpl {

	private double gear = 5;
	private String category = "manual" ;
	private Set<String> possibleGears;
	private Set<String> possibleCategories;
	
	public Transmission() {
		possibleGears = new HashSet<>();
		possibleGears.add("5");
		possibleGears.add("6");
		possibleGears.add("7");
		addProperty("gear", () -> getGear(), (p) -> setGear(p), possibleGears);
	
		possibleCategories = new HashSet<>();
		possibleCategories.add("manual");
		possibleCategories.add("automatic");
		possibleCategories.add("sequential");
		possibleCategories.add("converter");
		addProperty("category", () -> getCategory(), (c) -> setCategory(c), possibleCategories);
	
	}

	public String getGear() {
		return String.valueOf(this.gear);
	}

	public void setGear(String gear) {
		Objects.requireNonNull(gear, "gear cannot be null");
		this.gear = Double.valueOf(gear);
	}
	
	public void setCategory(String category) {
		Objects.requireNonNull(category, "cayegory cannot be null");
		this.category = category;
	}
	
	public String getCategory() {
		return this.category;
	}

	@Override
	public double getPrice() {
		switch (String.valueOf(gear)) {
		case "5.0":
			if(category.equals("manual"))
				return 129;
			else if(category.equals("automatic"))
				return 229;
			else if(category.equals("sequential"))
				return 329;
			return 429;
		case "6.0":
			if(category.equals("manual"))
				return 229;
			else if(category.equals("automatic"))
				return 329;
			else if(category.equals("sequential"))
				return 429;
			return 529;
		case "7.0":
			if(category.equals("manual"))
				return 329;
			else if(category.equals("automatic"))
				return 429;
			else if(category.equals("sequential"))
				return 529;
			return 629;
		default:
			return 0;
		}
	}

}
