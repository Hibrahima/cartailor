package com.impl;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import com.exceptions.ConflictingRuleException;
import com.exceptions.CrudException;
import com.helper.Utils;
import com.interfaces.Category;
import com.interfaces.Part;
import com.interfaces.PartType;

public class PartTypeImpl implements PartType {

	private Collection<Part> parts;
	private Collection<PartType> incompatibilities;
	private Collection<PartType> requirements;
	private String name;
	private String description;
	private Category category;
	private Class<? extends Part> reference;

	public PartTypeImpl(String name, String description, Category category, Class<? extends Part> reference) {
		this.name = name;
		this.description = description;
		this.category = category;
		this.reference  = reference;
		this.incompatibilities = new ArrayList<>();
		this.requirements = new ArrayList<>();
		this.parts = new ArrayList<>();
	}
	
	/**
	 * Constructs a part based on the reference of its part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * Implementation of the Type Instance / Object design pattern
	 * 
	 * @throws Exception if the reference cannot be instantiated 
	 * @return a part that has been constructed using the default constructor of any class extending Part
	 */
	public Part newInstance() throws Exception {
		Constructor< ?extends Part>  constructor = this.reference.getConstructor();
		return constructor.newInstance();
	}

	/**
	 * Gets the list of parts that are associated to a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the part type that is attached to a given part
	 */
	@Override
	public Iterator<Part> getParts() {
		return this.parts.iterator();
	}
	
	/**
	 * Gets the name of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the name a given part type
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the  name of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @throws NullPointerException if name is null
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retrieves the incompatible part types of [this]e
	 *
	 * @return an iterator on the retrieved incompatible parts
	 */
	@Override
	public Iterator<PartType> getIncompatibleParts() {
		return this.incompatibilities.iterator();
	}

	/**
	 * Retrieves the list of part types that are required by [this]
	 *
	 * @return an iterator on the retrieved required parts
	 */
	@Override
	public Iterator<PartType> getRequiredParts() {
		return this.requirements.iterator();
	}

	/**
     * Adds a set of part types to [this] as incompatible with [this]
     *
     * @param incompatibilities the iterator that contains the incompatible part types to add to [this]
     * @throws NullPointerException if incompatibilities is null
     * @throws ConflictingRuleException if the requirement list of [this] contains an element present in the incompatibilities iterator
     * @throws ConflictingRuleException if the requirement list of an element present in the incompatibilities iterator contains [this]
     * @throws CrudException if the incompatibility list of [this] already contains an element present in the incompatibilities iterator
     * @throws CrudException if an element present in the incompatibilities iterator is equals to [this]
     */
	@Override
	public void addIncompatibilities(Iterator<PartType> incompatibilities) throws ConflictingRuleException, CrudException {
		Objects.requireNonNull(incompatibilities, "the submited incompatibility list cannot be null");
		List<PartType> incom = Utils.convertIteratorToList(incompatibilities);
		List<PartType> currentPartTypeRequirements = new ArrayList<>();
		for(PartType inc : incom) {
			currentPartTypeRequirements = Utils.convertIteratorToList(inc.getRequiredParts());
			if(this.requirements.contains(inc))
				throw new ConflictingRuleException(this.getName()+" require "+inc.getName()+", hence "+inc.getName()+" should not be incompatible");
			if(currentPartTypeRequirements.contains(this))
				throw new ConflictingRuleException(this.name+" require "+inc.getName()+", hence "+inc.getName()+" should not be incompatible");
			if(this.incompatibilities.contains(inc))
				throw new CrudException(this.name+" is already incompatible with "+inc.getName()+" no duplicate is allowed");
			if(inc.equals(this))
				throw new CrudException(this.name+" cannot be incompatible with itself");
			this.incompatibilities.add(inc);
			System.out.println(inc.getName()+" has been added to "+this.name+" as an incompatible part");
				
		}
	}

	/**
     * Adds a set of part types to [this] as required by [this]
     *
     * @param requirements the iterator that contains the required part types to add to [this]
     * @throws NullPointerException if requirements is null
     * @throws ConflictingRuleException if the incompatibility list of [this] contains an element present in the requirements iterator
     * @throws ConflictingRuleException if the incompatibility list of an element present in the requirements iterator contains [this]
     * @throws CrudException if the requirement list of [this] already contains an element present in the requirements iterator
     * @throws CrudException if an element present in the requirements iterator is equals to [this]
     */
	@Override
	public void addRequirements(Iterator<PartType> requirements) throws ConflictingRuleException, CrudException {
		Objects.requireNonNull(requirements, "the submited requirement list cannot be null");
		List<PartType> requi = Utils.convertIteratorToList(requirements);
		List<PartType> currentPartTypeIncompatibilities = new ArrayList<>();
		for(PartType req : requi) {
			currentPartTypeIncompatibilities = Utils.convertIteratorToList(req.getIncompatibleParts());
			if(this.incompatibilities.contains(req))
				throw new ConflictingRuleException(this.name+" is incompatible with "+req.getName()+", hence "+req.getName()+" should not be required");
			if(currentPartTypeIncompatibilities.contains(this)) 
				throw new ConflictingRuleException(this.name+" is incompatible with "+req.getName()+", hence "+req.getName()+" should not be required");
			if(this.requirements.contains(req))
				throw new CrudException(req.getName()+" is already required by "+this.name+" no duplicate is allowed");
			if(req.equals(this))
				throw new CrudException(this.getName()+" cannot be required by itself");
			this.requirements.add(req);
			System.out.println(req.getName()+" has been added to "+this.name+" as a required part");
		}
		
	}

	/**
	* Removes a set of incompatible part types from [this]
	*
	* @param incompatibilities the iterator that contains the incompatible part types to remove from [this]
	* @throws NullPointerException if incompatibilities is null
	* @throws CrudException if the incompatibility list of [this] does not contain the current element present in the incompatibilities iterator
	*/
	@Override
	public void removeIncompatibilities(Iterator<PartType> incompatibilities) throws CrudException {
		Objects.requireNonNull(incompatibilities, " incompatibilities list cannot be null");
		List<PartType> incom = Utils.convertIteratorToList(incompatibilities);
		for(PartType pt : incom) {
			if(!this.incompatibilities.contains(pt))
				throw new CrudException("Removing "+pt.getName()+", seriously? "+pt.getName()+" is not present in the incompatibility list of "+this.name);
			this.incompatibilities.remove(pt);
		}
	}

	/**
	* Removes a set of required part types from [this]
	*
	* @param requirements the iterator that contains the required part types to remove from [this]
	* @throws NullPointerException if requirements is null
	* @throws CrudException if the requirement list of [this] does not contain the current element present in the requirements iterator
	*/
	@Override
	public void removeRequirements(Iterator<PartType> requirements) throws CrudException {
		Objects.requireNonNull(incompatibilities, " incompatibilities list cannot be null");
		List<PartType> requi = Utils.convertIteratorToList(requirements);
		for(PartType pt : requi) {
			if(!this.requirements.contains(pt))
				throw new CrudException("Removing "+pt.getName()+", seriously? "+pt.getName()+" is not present in the requirement list of "+this.name);
			this.requirements.remove(pt);
		}
		
	}

	/**
	 * Gets the description of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the description  of a given part type
	 */
	@Override
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the  description of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @throws NullPointerException if description is null
	 */
	@Override
	public void setDescription(String description) {
		Objects.requireNonNull(description, "description cannot be null");
		this.description = description;
	}


	/**
	 * Adds a part to a given part type
	 * A part type should not contain the same part more than once
	 * 
	 * @param p the part to add to a given part type
	 * @throws NullPointerException if p is null 
	 * @throws CrudException if the part type already contains p 
	 */
	@Override
	public void addPart(Part p) throws CrudException {
		Objects.requireNonNull(p, "part cannot be null");
		if(this.parts.contains(p))
			throw new CrudException("bbbb"+this.name+" already contains this part");
		this.parts.add(p);
	}

	/**
	 * Removes a part from a given part type
	 * A part type should not contain the same part more than once
	 * 
	 * @param p the part to remove from a given part type
	 * @throws NullPointerException if p is null 
	 * @throws CrudException if the part type does not contains p 
	 */
	@Override
	public void removePart(Part p) throws CrudException {
		Objects.requireNonNull(p, "part cannot be null");
		if(!this.parts.contains(p))
			throw new CrudException(this.getName()+" already does not contains this part");
		this.parts.remove(p);
	}

	/**
	 * Gets the category of a given part type
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * A category may have multiple part types
	 * 
	 * @return the category  of a given part type
	 */
	@Override
	public Category getCategory() {
		// TODO Auto-generated method stub
		return this.category;
	}
	
	

}
