package com.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import com.interfaces.Category;
import com.interfaces.Part;
import com.interfaces.PartType;

public enum Categories implements Category {

	ENGINE("Engine"), TRANSMISSION("Transmission"), INTERIOR("Interior"), EXTERIOR("Exterior");

	private String name;
	private Collection<PartType> partTypes;

	private Categories(String name) {
		this.name = name;
		this.partTypes = new ArrayList<>();
	}

	/**
	 * Retrieves the name of a given category
	 *
	 * @return the name of that given category
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * Retrieves the list of part types that are associated to a given category
	 *
	 * @return an iterator on retrieved part types
	 */
	@Override
	public Iterator<PartType> getPartTypes() {
		return this.partTypes.iterator();
	}

	/**
	 * Retrieves the list of parts that are associated to a given part type that at
	 * its turn is associated to a given category
	 *
	 * @return an iterator on the retrieved parts
	 */
	@Override
	public Iterator<Part> getParts() {
		List<Part> parts = new ArrayList<>();
		for (PartType pt : this.partTypes) {
			Iterator<Part> it = pt.getParts();
			while (it.hasNext())
				parts.add(it.next());
		}
		return parts.iterator();
	}

	/**
	 * add a particular part type to a given category
	 *
	 * @param p the part type to add
	 * @throws NullPointerException if p is null
	 */
	@Override
	public void addPartType(PartType p) {
		Objects.requireNonNull(p, "part type cannot be null");
		if (this.partTypes.contains(p))
			throw new IllegalArgumentException("xxxxxxcategory " + this.name + " already contains " + p.getName());
		this.partTypes.add(p);
	}

}
