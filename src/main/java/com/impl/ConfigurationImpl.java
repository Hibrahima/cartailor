package com.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import com.helper.Utils;
import com.interfaces.Configuration;
import com.interfaces.Element;
import com.interfaces.Observer;
import com.interfaces.Part;
import com.interfaces.PartType;
import com.interfaces.Visitor;

public class ConfigurationImpl implements Configuration, Element {

	private List<Observer> registeredObservers;
	private List<Part> parts;

	public ConfigurationImpl() {
		this.parts = new ArrayList<>();
		this.registeredObservers = new ArrayList<>();
	}

	/**
	 * Retrieves the incompatible part types of a given part type
	 *
	 * @param partType the part type whose incompatible parts are retrieved
	 * @return an iterator on the retrieved incompatible parts
	 * @throws NullPointerException if partType is null
	 */
	@Override
	public Iterator<PartType> getIncompatibleParts(PartType partType) {
		Objects.requireNonNull(partType, "part type cannot be null");
		return partType.getIncompatibleParts();
	}

	/**
	 * Retrieves the list of part types that are required by a given part type
	 *
	 * @param partType the part type whose required parts are retrieved
	 * @return an iterator on the retrieved required parts
	 * @throws NullPointerException if partType is null
	 */
	@Override
	public Iterator<PartType> getRequiredParts(PartType partType) {
		Objects.requireNonNull(partType, "part type cannot be null");
		return partType.getRequiredParts();
	}

	/**
	 * Checks that whether or not a configuration is complete
	 * A configuration is complete iff it is valid and contains all 4 categories
	 * 
	 * @return a boolean that indicates the completeness of the configuration 
	 */
	@Override
	public boolean isComplete() {
		Set<String> categoryNames = new HashSet<>();
		for (Part p : this.parts) {
			categoryNames.add(p.getPartType().getCategory().getName());
		}

		if (isValid())
			return categoryNames.size() == 4;

		return false;
	}

	/**
	 * Checks that whether or not a configuration is valid
	 * A configuration is valid iff does not contain any incompatible parts of the parts present in the configuration
	 * and iff it contains all required parts of each part present in the configuration
	 * 
	 * @return a boolean that indicates the validness of the configuration 
	 */
	@Override
	public boolean isValid() {
		List<PartType> currentPartTypeIncompatibilities;
		List<PartType> currentPartTypeRequirements;
		List<PartType> partTypes = new ArrayList<>();
		for (Part part : this.parts) {
			partTypes.add(part.getPartType());
		}
		for (PartType currentPartType : partTypes) {
			currentPartTypeIncompatibilities = Utils.convertIteratorToList(this.getIncompatibleParts(currentPartType));
			currentPartTypeRequirements = Utils.convertIteratorToList(this.getRequiredParts(currentPartType));
			for (PartType inc : currentPartTypeIncompatibilities) {
				if (partTypes.contains(inc))
					return false;
			}

			for (PartType req : currentPartTypeRequirements) {
				if (!partTypes.contains(req))
					return false;
			}
		}
		return true;
	}
	
	
	/**
	 * Displays the state of the configuration 
	 * A configuration should not register the same observer more tan once
	 */
	private void displayConfigurationState() {
		if (isValid()) {
			System.out.println("The configuration is valid");
			if (isComplete())
				System.out.println("The configuration is complete");
		} else
			System.out.println("The configuration is not valid");
	}

	/**
	 * Adds a part to a configuration
	 * A configuration should not contain the same part more than once
	 * Displays the validness and the completeness of the configuration 
	 * Notifies all registered observers
	 * 
	 * @param part the part to add to the configuration
	 * @throws NullPointerException if part is null 
	 * @throws IllegalArgumentException if the configuration already contains part 
	 */
	@Override
	public void addPart(Part part) {
		Objects.requireNonNull(part, "part cannot be null");
		if (this.parts.contains(part))
			throw new IllegalArgumentException(part.getName() + " is already present in the configuration");
		this.parts.add(part);
		this.notifyRegisteredObservers();
		System.out.println(part.getName() + " has been added to the configuration");
		displayConfigurationState();
	}

	/**
	 * Removes a part from a configuration
	 * A configuration should not contain the same part more than once
	 * Displays the validness and the completeness of the configuration 
	 * Notifies all registered observers
	 * 
	 * @param part the part to remove from the configuration
	 * @throws NullPointerException if part is null 
	 * @throws IllegalArgumentException if the configuration does not contains part 
	 */
	@Override
	public void removePart(Part part) {
		Objects.requireNonNull(part, "part type cannot be null");
		if (!this.parts.contains(part))
			throw new IllegalArgumentException(part.getName() + " is not present in the configuration");
		this.parts.remove(part);
		this.notifyRegisteredObservers();
		System.out.println(part.getName() + " has been removed from the configuration");
		displayConfigurationState();
	}

	/**
	 * Registers an observer
	 * A configuration should not register the same observer more tan once 
	 * 
	 * @param observer the observer to register
	 * @throws NullPointerException if observer is null
	 * @throws IllegalArgumentException if observer is already registered
	 */
	@Override
	public void register(Observer observer) throws IllegalArgumentException {
		Objects.requireNonNull(observer, "observer cannot be null");
		if (isRegistered(observer))
			throw new IllegalArgumentException("this observer is already registered");
		this.registeredObservers.add(observer);

	}

	/**
	 * Unregisters an observer
	 * A configuration should not register the same observer more tan once 
	 * 
	 * @param observer the observer to unregister
	 * @throws NullPointerException if observer is null
	 * @throws IllegalArgumentException if observer is not registered
	 */
	@Override
	public void unregister(Observer observer) throws IllegalArgumentException {
		Objects.requireNonNull(observer, "observer cannot be null");
		if (!isRegistered(observer))
			throw new IllegalArgumentException("this observer is not registered");
		this.registeredObservers.remove(observer);

	}

	/**
	 * Checks that whether or not a given observer is registered 
	 * A configuration should not register the same observer more tan once 
	 * 
	 * @param observer the observer to test
	 * @throws NullPointerException if observer is null
	 * @return a boolean indicating whether ot not observer is registered 
	 */
	@Override
	public boolean isRegistered(Observer observer) {
		Objects.requireNonNull(observer, "observer cannot be null");
		return this.registeredObservers.contains(observer);
	}

	/**
	 * Updates all registered observers 
	 * A configuration should not register the same observer more tan once 
	 */
	private void notifyRegisteredObservers() {
		for (Observer o : registeredObservers) {
			o.update(this);
		}
	}

	/**
	 * Retrieves the list of parts that have been added to a configuration
	 * A configuration should not contain the same part more than once
	 * 
	 * @return an iterator on the parts that are present in the configuration 
	 */
	@Override
	public Iterator<Part> getParts() {
		return this.parts.iterator();
	}

	/**
	 * Retrieves the list of observers (configurators) that are watching (registered) the configuration
	 * A configuration should not contain the same part more than once
	 * 
	 * @return an iterator on the observers that are watching the configuration 
	 */
	@Override
	public Iterator<Observer> getRegisteredObservers() {
		return this.registeredObservers.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getPrice() {
		double price = 0;
		if(isValid()) {
			for(Part p : this.parts)
				price += ((PartImpl)p).getPrice();
			
			return price;
		}
		System.out.println("The configuration being invalid, the price is "+price+" euros");
		return price;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void accept(Visitor v) {
		String state = isComplete() ? "complete" : "not complete";
		Set<String> categories = new HashSet<>();
		if(!isComplete()) {
			System.out.println("The configuration is not complete, hence showing its description is not allowed");
			return;
		}
		System.out.println("<html>");
		System.out.println("  <header>");
		System.out.println("   <title> Description of my configuration </title>");
		System.out.println("  </header>");
		System.out.println("  <body>");
	    System.out.println("    <parts>");
		for(Element e : this.parts) {
			e.accept(v);
			categories.add(((PartImpl)e).getPartType().getCategory().getName());
		}
					
						
		System.out.println("    </parts>");
		System.out.println("  </body>");
		System.out.println("  <footer>");
		System.out.println("    <price> "+getPrice()+" </price>");
		System.out.println("    <number> "+this.parts.size()+" parts </price>");
		System.out.println("    <state> "+state+" </price>");
		System.out.println("    <categories>");
		for(String cat : categories) {
			System.out.println("     <name> "+cat+" </name>");
		}
		System.out.println("    </categories>");
		System.out.println("  </footer>");
		System.out.println("</html>");
	   
	}


}
