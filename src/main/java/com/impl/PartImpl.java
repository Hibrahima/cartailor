package com.impl;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;
import com.interfaces.Part;
import com.interfaces.PartType;
import com.interfaces.Visitor;

public class PartImpl implements Part{
	
	private PartType partType;
	private String name;

	private class Property{
		public final Supplier<String> getter;
		public final Consumer<String> setter;
		public final Set<String>possibleValues;
		
		public Property(Supplier<String> getter, Consumer<String> setter, Set<String> possibleValues) {
			this.setter = setter;
			this.getter = getter;
			this.possibleValues = possibleValues;
		}
	}
	
	private Map<String, Property> properties = new HashMap<>();
	
	/**
	 * adds a property to a given part
	 * @param name the name of the property to set
	 * @param getter the getter to use for that property - is a lambda expression
	 * @param setter the setter to use for that property - is a lambda expression
	 * @param possibleValues the set of possible values for that property - does not allow duplicates
	 */
	protected void addProperty(String name, Supplier<String> getter, Consumer<String> setter, Set<String> possibleValues) {
		Objects.requireNonNull(name, "property name cannot be null");
		//Objects.requireNonNull(getter, "supplier getter cannot be null");
		//Objects.requireNonNull(setter, "consumer setter cannot be null");
		properties.put(name, new Property(getter, setter, possibleValues));
	}
	
	
	/**
	 * Gets the name of a given part 
	 * 
	 * @return the name of a given part
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the part type which is associated to a given part
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the part type that is attached to a given part
	 */
	@Override
	public PartType getPartType() {
		return this.partType;
	}

	/**
	 * Sets the part type which is associated to a given part
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @param p the part type to associate to a given part
	 * @throws NullPointerException if p is null
	 */
	@Override
	public void setPartType(PartType p) {
		Objects.requireNonNull(p, "part type cannot be null");
		this.partType = p;
	}

	/**
	 * Sets the name of a given part 
	 * 
	 * @param name the name to set
	 */
	@Override
	public void setName(String name) {
		Objects.requireNonNull(name);
		this.name = name;
		
	}


	/**
	 * Returns an immutable set of property names supported by the property manager
	 * 
	 * @return that immutable set
	 */
	@Override
	public Set<String> getPropertyNames() {
		return Collections.unmodifiableSet(properties.keySet());
	}

	/**
	 * Returns the immutable set of discrete string values for a given property
	 * Returns an empty set of for properties that have a non explicit set of possible values
	 * Returns an empty set for non existing property name
	 * 
	 * @param name a non-null string reference
	 * @return an immutable set
	 */
	@Override
	public Set<String> getAvailablePropertyValues(String name) {
		Objects.requireNonNull(name, "property name cannot be null");
		if(properties.containsKey(name))
			return Collections.unmodifiableSet(properties.get(name).possibleValues);
		
		return Collections.emptySet();
	}

	/**
	 * Return the optional value of a property
	 * If the object does not support the supplied property,an empty optional is returned
	 * 
	 * @param name the property name to read
	 * @return the optional of that property
	 */
	@Override
	public Optional<String> getProperty(String name) {
		Objects.requireNonNull(name, "property name cannot be null");
		if(properties.containsKey(name))
			return Optional.of(properties.get(name).getter.get());
		
		return Optional.empty();
	}

	/**
	 * Sets the value of a given property
	 *
	 * @param name the property name to set
	 * @param value the value of the property to set
	 * @throws IllegalArgumentException  if there is no such property, or if it's not writable, or if the value is invalid
	 * @throws IllegalArgumentException if value is not present in the possible values list supported by this property
	 */
	@Override
	public void setProperty(String name, String value) {
		Objects.requireNonNull(name, "property name cannot be null");
		Objects.requireNonNull(value, "property value cannot be null");
		if(properties.containsKey(name) && properties.get(name).setter != null 
				&& properties.get(name).possibleValues.contains(value))
			properties.get(name).setter.accept(value);
		else
			throw new IllegalArgumentException("bad property name or value : "+name);
	}


	/**
	 * Returns the price of a given part
	 * Prices are defined at the lowest level (sub classes eg Exterior, Interior, ...) based on the color of the part
	 * Returns 0 (zero) as the basic price if nothing is supplied
	 * @return that price
	 */
	public double getPrice() {
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void accept(Visitor v) {
		v.visit(this);
		
	}

	/**
	 * Gets the color name of a given part
	 * Colors are defined at the lowest level (sub classes eg Exterior, Interior, ...)
	 * 
	 * @return that color
	 */
	public String getColor() {
		return "undefined";
	}


	/**
	 * Gets the description of a given part
	 * The description is part of the html description of the configuration 
	 * Return undefined as the basic description if nothing specific is supplied
	 * 
	 * @return that description
	 */
	public String getDescription() {
		return "undefined";
	}

}
