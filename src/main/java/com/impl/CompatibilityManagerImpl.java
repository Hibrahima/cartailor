package com.impl;

import java.util.Iterator;
import java.util.Objects;

import com.exceptions.ConflictingRuleException;
import com.exceptions.CrudException;
import com.interfaces.CompatibilityManager;
import com.interfaces.PartType;

public class CompatibilityManagerImpl implements CompatibilityManager {

	/**
	 * Retrieves the incompatible part types of a given part type
	 *
	 * @param partType the part type whose incompatible parts are retrieved
	 * @return an iterator on the retrieved incompatible parts
	 * @throws NullPointerException if partType is null
	 */
	@Override
	public Iterator<PartType> getIncompatibleParts(PartType partType) {
		Objects.requireNonNull(partType, "part type cannot be null");
		return partType.getIncompatibleParts(); 
	}

	/**
	 * Retrieves the list of part types that are required by a given part type
	 *
	 * @param partType the part type whose required parts are retrieved
	 * @return an iterator on the retrieved required parts
	 * @throws NullPointerException if partType is null
	 */
	@Override
	public Iterator<PartType> getRequiredParts(PartType partType) {
		Objects.requireNonNull(partType, "part type cannot be null");
		return partType.getRequiredParts();
	}

	/**
     * Adds a set of part types to reference as incompatible with reference
     *
     * @param reference the part type whose incompatibility list is set
     * @param incompatibilities the iterator that contains the incompatible part types to add to reference
     * @throws NullPointerException if reference is null
     * @throws NullPointerException if incompatibilities is null
     * @throws ConflictingRuleException if the requirement list of reference contains an element present in the incompatibilities iterator
     * @throws ConflictingRuleException if the requirement list of an element present in the incompatibilities iterator contains reference
     * @throws CrudException if the incompatibility list of reference already contains an element present in the incompatibilities iterator
     * @throws CrudException if an element present in the incompatibilities iterator is equals to reference
     * @see com.impl.PartTypeImpl#addIncompatibilities(Iterator)
     */
	@Override
	public void addIncompatibilities(PartType reference, Iterator<PartType> incompatibilities)
			throws ConflictingRuleException, CrudException {
		Objects.requireNonNull(reference, "part type cannot be null");
		Objects.requireNonNull(incompatibilities, "the submited incompatibility list cannnot be null");
		reference.addIncompatibilities(incompatibilities);

	}

	/**
     * Adds a set of part types to reference as required by reference
     *
     * @param reference the part type whose requirement list is set
     * @param requirements the iterator that contains the required part types to add to reference
     * @throws NullPointerException if reference is null
     * @throws NullPointerException if requirements is null
     * @throws ConflictingRuleException if the incompatibility list of reference contains an element present in the requirements iterator
     * @throws ConflictingRuleException if the incompatibility list of an element present in the requirements iterator contains reference
     * @throws CrudException if the requirement list of reference already contains an element present in the requirements iterator
     * @throws CrudException if an element present in the requirements iterator is equals to reference
     * @see com.impl.PartTypeImpl#addRequirements(Iterator)
     */
	@Override
	public void addRequirements(PartType reference, Iterator<PartType> requirements)
			throws ConflictingRuleException, CrudException {
		Objects.requireNonNull(reference, "part type cannot be null");
		Objects.requireNonNull(requirements, "the submited requirement list cannnot be null");
		reference.addRequirements(requirements);
	}

	/**
	* Removes a set of incompatible part types from reference
	*
	* @param reference the part type whose incompatibility list is updated
	* @param incompatibilities the iterator that contains the incompatible part types to remove from reference
	* @throws NullPointerException if reference is null
	* @throws NullPointerException if incompatibilities is null
	* @throws CrudException if the incompatibility list of reference does not contain the current element present in the incompatibilities iterator
	* @see com.impl.PartTypeImpl#removeIncompatibilities(Iterator)
	*/
	@Override
	public void removeIncompatibilities(PartType reference, Iterator<PartType> incompatibilities)
			throws CrudException {
		Objects.requireNonNull(reference, "part type cannot be null");
		Objects.requireNonNull(incompatibilities, "the submited incompatibility list cannnot be null");
		reference.removeIncompatibilities(incompatibilities);
	}

	/**
	* Removes a set of required part types from reference
	*
	* @param reference the part type whose requirement list is updated
	* @param requirements the iterator that contains the required part types to remove from reference
	* @throws NullPointerException if reference is null
	* @throws NullPointerException if requirements is null
	* @throws CrudException if the requirement list of reference does not contain the current element present in the requirements iterator
	* @see com.impl.PartTypeImpl#removeRequirements(Iterator)
	*/
	@Override
	public void removeRequirements(PartType reference, Iterator<PartType> requirements)
			throws  CrudException {
		Objects.requireNonNull(reference, "part type cannot be null");
		Objects.requireNonNull(requirements, "the rsubmited equirement  list cannnot be null");
		reference.removeRequirements(requirements);

	}

}
