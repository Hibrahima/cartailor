package com.impl;

import com.interfaces.Visitor;
import com.internals.Engine;
import com.internals.Interior;
import com.internals.Transmission;

public class ElementVisitor implements Visitor {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(PartImpl p) {
		System.out.println();
		System.out.println("     <part>");
		System.out.println("      <name> "+p.getName()+" </name>");
		System.out.println("      <price> "+p.getPrice()+" </price>"); 
		if(!p.getColor().equals("undefined"))
			System.out.println("      <color> "+p.getColor()+" </color");
		if(!p.getDescription().equals("undefined"))
			System.out.println("      <description> "+p.getDescription()+" </description>");
		if(p.getPartType().getCategory().getName().equals("Engine")) {
			System.out.println("    <power> "+((Engine)p).getPower()+" KW </power>");
		}
		else if(p.getPartType().getCategory().getName().equals("Transmission"))
			System.out.println("    <gear> "+((Transmission) p).getGear()+" </gear>");
		else if(p.getPartType().getCategory().getName().equals("Interior"))
			System.out.println("    <type> "+((Interior) p).getType()+" </type>");
		
		System.out.println("      <category> "+p.getPartType().getCategory().getName()+" </category");
		System.out.println("      <part type>");
		System.out.println("        <name> "+p.getPartType().getName()+" </name>");
		System.out.println("        <description> "+p.getPartType().getDescription()+" </description>");
		System.out.println("      </part type>");
		System.out.println("     </part>");
		//System.out.println();

	}

}
