package com.impl;


import java.util.Objects;

import com.interfaces.Configuration;
import com.interfaces.Configurator;
import com.interfaces.Observable;

public class ConfiguratorImpl implements Configurator {
	
	private Configuration configuration;

	/**
	 * Gets the current configuration
	 * @return the current configuration
	 */
	@Override
	public Configuration getConfiguration() {
		return this.configuration;
	}

	/**
	 * Updates observer so that it receives the current configuration
	 * Updates the related observer with the submited observable object
	 */
	@Override
	public void update(Observable observable) { 
		Objects.requireNonNull(observable, "category cannot be null");
		this.configuration = (Configuration) observable; 
	}

}
