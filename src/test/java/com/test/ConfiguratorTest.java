package com.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import com.impl.ConfigurationImpl;
import com.impl.ConfiguratorImpl;
import com.interfaces.Configuration;
import com.interfaces.Configurator;
import com.interfaces.Observer;
import com.interfaces.Part;

public class ConfiguratorTest {

	private List<Configurator> configurators;
	private Configuration configuration;
	private Part eg100Gaoline;
	private Part xcClassic;
	private Part eh120Hybrid;
	private Part tc120Normal;
	private Part inStandard;

	@BeforeEach
	public void setup() throws Exception {
		configuration = new ConfigurationImpl();
		new BuilderFactory();
		Collection<Part> eg100Parts = new ArrayList<>();
		eg100Gaoline = BuilderFactory.buildPartEG100Gasoline();
		eg100Parts.add(eg100Gaoline);


		Collection<Part> xcParts = new ArrayList<>();
		xcClassic = BuilderFactory.buildPartXCClaasic();
		xcParts.add(xcClassic);

		Collection<Part> eh120Parts = new ArrayList<>();
		eh120Hybrid = BuilderFactory.buildPartEH120Hybrid();
		eh120Parts.add(eh120Hybrid);

		Collection<Part> tc120Parts = new ArrayList<>();
		tc120Normal = BuilderFactory.buildPartTC120Normal();
		tc120Parts.add(tc120Normal);

		

		Collection<Part> inParts = new ArrayList<>();
		inStandard = BuilderFactory.buildPartInStandard();
		inParts.add(inStandard);

		BuilderFactory.buildPartType(BuilderFactory.eg100, new ArrayList<>(), new ArrayList<>(), eg100Parts);
		BuilderFactory.buildPartType(BuilderFactory.xc, BuilderFactory.xcIncompatibilities, new ArrayList<>(), xcParts);
		BuilderFactory.buildPartType(BuilderFactory.eh120, new ArrayList<>(), BuilderFactory.eh120Requiremets, eh120Parts);
		BuilderFactory.buildPartType(BuilderFactory.tc120, new ArrayList<>(), BuilderFactory.tc120Requiremets, tc120Parts);
		BuilderFactory.buildPartType(BuilderFactory.ts6, new ArrayList<>(), new ArrayList<>(), tc120Parts);
		BuilderFactory.buildPartType(BuilderFactory.in, new ArrayList<>(), new ArrayList<>(), inParts);

		int random = generateRandonNumber(4, 20);
		configurators = new ArrayList<>();
		for (int i = 0; i < random; i++) {
			Configurator conf = mock(Configurator.class);
			configurators.add(conf);
			configuration.register(conf);
		}

	}

	private int generateRandonNumber(int min, int max) {
		int x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	@Test
	@DisplayName("get configuration")
	@Tag("all_observers_update")
	public void testObserversAndConfiguration() {
		System.out.println("----------- Configurator Test / Test Observers and Configuartor vs Configuration ## BEGIN -----------");
		configuration.addPart(eg100Gaoline);
		
		/****** verifying that all registered observers have received the updates ******/
		for (Observer o : configurators) {
			verify(o).update(configuration);
		}

		/****** transform the observers from mock object to concrete objects ******/
		int counter = 0;
		for (Configurator c : configurators) {
			c = new ConfiguratorImpl();
			c.update(configuration);
			configurators.set(counter, c);
			assertTrue(c.getConfiguration() == configuration);
			counter++;
		}

		/****** adding a part and testing that the observers have been really notified ******/
		/****** all registered observers should have the updated configuration interface at this point ******/
		configuration.addPart(eh120Hybrid);
		assertFalse(configuration.isValid());
		for (Configurator c : configurators) {
			assertFalse(c.getConfiguration().isValid());
		}

		configuration.addPart(tc120Normal);
		assertTrue(configuration.isValid());
		for (Configurator c : configurators) {
			assertTrue(c.getConfiguration().isValid());
		}

		configuration.addPart(xcClassic);
		assertTrue(configuration.isValid());
		assertFalse(configuration.isComplete());
		for (Configurator c : configurators) {
			assertTrue(c.getConfiguration().isValid());
			assertFalse(c.getConfiguration().isComplete());
		}

		configuration.addPart(inStandard);
		assertTrue(configuration.isValid());
		assertTrue(configuration.isComplete());
		for (Configurator c : configurators) {
			assertTrue(c.getConfiguration().isValid());
			assertTrue(c.getConfiguration().isComplete());
		}

	
		System.out.println("----------- Configurator Test / Test Observers and Configuartor vs Configuration ## END -----------");
	}
}
