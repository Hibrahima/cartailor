package com.test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import com.helper.Colors;
import com.impl.ConfigurationImpl;
import com.impl.ElementVisitor;
import com.impl.PartImpl;
import com.interfaces.Part;
import com.interfaces.Visitor;

public class V2ConfigurationTest {
	
	private Part eg100Gaoline;
	private Part tm5Manual;
	private Part xcClassic;
	private Part eh120Hybrid;
	private Part tc120Normal;
	private Part ts6Sequntial;
	private Part tsf7Sequntial;
	private Part inStandard;
	
	
	
	@BeforeEach
	public void setup() throws Exception {
		new BuilderFactory();
		
		Collection<Part> eg100Parts = new ArrayList<>();
		eg100Gaoline = BuilderFactory.buildPartEG100Gasoline();
		eg100Parts.add(eg100Gaoline);
		
		Collection<Part> tm5Parts = new ArrayList<>();
		tm5Manual = BuilderFactory.buildPartTM5Manual();
		tm5Parts.add(tm5Manual);
		
		Collection<Part> xcParts = new ArrayList<>();
		 xcClassic = BuilderFactory.buildPartXCClaasic();
		xcParts.add(xcClassic);
		
		Collection<Part> eh120Parts = new ArrayList<>();
		eh120Hybrid = BuilderFactory.buildPartEH120Hybrid();
		eh120Parts.add(eh120Hybrid);
		
		Collection<Part> tc120Parts = new ArrayList<>();
		 tc120Normal = BuilderFactory.buildPartTC120Normal();
		tc120Parts.add(tc120Normal);
		
		Collection<Part> ts6Parts = new ArrayList<>();
		ts6Sequntial = BuilderFactory.buildPartTS6Sequentail();
		ts6Parts.add(ts6Sequntial);
		
		Collection<Part> tsf7Parts = new ArrayList<>();
		tsf7Sequntial = BuilderFactory.buildPartTSF7Sequentail();
		tsf7Parts.add(tsf7Sequntial);
		
		Collection<Part> inParts = new ArrayList<>();
		inStandard = BuilderFactory.buildPartInStandard();
		inParts.add(inStandard);
		
		BuilderFactory.buildPartType(BuilderFactory.eg100, new ArrayList<>(), new ArrayList<>(), eg100Parts);
		BuilderFactory.buildPartType(BuilderFactory.tm5, new ArrayList<>(), new ArrayList<>(), tm5Parts);
		BuilderFactory.buildPartType(BuilderFactory.xc, BuilderFactory.xcIncompatibilities, new ArrayList<>(), xcParts);
		BuilderFactory.buildPartType(BuilderFactory.eh120, new ArrayList<>(), BuilderFactory.eh120Requiremets, eh120Parts);
		BuilderFactory.buildPartType(BuilderFactory.tc120, new ArrayList<>(), BuilderFactory.tc120Requiremets, tc120Parts);
		BuilderFactory.buildPartType(BuilderFactory.ts6, new ArrayList<>(), new ArrayList<>(), tc120Parts);
		BuilderFactory.buildPartType(BuilderFactory.tsf7, BuilderFactory.tsf7Incompatibilities, new ArrayList<>(), tsf7Parts);
		BuilderFactory.buildPartType(BuilderFactory.in, new ArrayList<>(), new ArrayList<>(), inParts);
		
	}
	
	
	@Test
	@DisplayName("test prices")
	public void testPartsPrices() {
		
		System.out.println("----------- Configuration V2 Test / Test patrs price ## BEGIN -----------");
		
		/******************************** Exterior ********************************/
		
		/****** builds xc red from the factory ******/
		PartImpl xcClassicRed = BuilderFactory.buildPartXCRed();
		
		/****** asserting that xc red has the right price ******/
		assertEquals(4000d, (xcClassicRed).getPrice());
		
		/****** builds xc magenta from the factory ******/
		 PartImpl xcMagenta = BuilderFactory.buildPartXCMagenta();
		 
		/****** asserting that xc magenta has the right price ******/
		assertEquals(2700d, xcMagenta.getPrice());
		 
		/****** builds xm metallic purple from the factory ******/
		PartImpl xmMetallicPurple = BuilderFactory.buildPartXMMetallicPurple();
		
		/****** asserting that xm metallic purple has the right price ******/
		assertEquals(799d, xmMetallicPurple.getPrice());
		
		
		/******************************** Interior ********************************/
		
		/****** builds in high end from the factory ******/
		PartImpl inHighEnd = BuilderFactory.buildPartInHighEnd();
		
		/****** asserting that in high end has the right price ******/
		assertEquals(3200d, inHighEnd.getPrice());
		
		/****** builds in sport finish and asserts that it has the right price ******/
		assertEquals(2875d, BuilderFactory.buildPartInSportFinish().getPrice());
		
		/******************************** Engine ********************************/
		
		/**** builds and tests eg100 gasoline 120 kw ****/
		assertEquals(499d, BuilderFactory.buildPartEgGasoline100KW().getPrice());
		
		/**** builds and tests eg133 gasoline 133 kw ****/
		assertEquals(599d, BuilderFactory.buildPartEgGasoline133KW().getPrice());
		
		/**** builds and tests ed110 diesel 110 kw ****/
		assertEquals(429d, BuilderFactory.buildPartEgDiesel110KW().getPrice());
		
		/**** builds and tests eh120 hybrid (gasoline) 120 kw  ****/
		assertEquals(559, BuilderFactory.buildPartEgHybrid120KW().getPrice());
		
		/************************************* Transmission *****************************************/
		
		/**** builds and tests tm5 manual 5 gears  ****/
		assertEquals(129d, BuilderFactory.buildPartTM5Manual5Gear().getPrice());
		
		/**** builds and tests tm6 manual 6 gears  ****/
		assertEquals(229d, BuilderFactory.buildPartTM6Manual6Gear().getPrice());
		
		/**** builds and tests ta5 automatic 5 gears  ****/
		assertEquals(229d, BuilderFactory.buildPartTA5Automatic5Gear().getPrice());
		
		/**** builds and tests ts6 sequential 6 gears  ****/
		assertEquals(429d, BuilderFactory.buildPartTS6Sequential6Gear().getPrice());
		assertNotEquals(428d, BuilderFactory.buildPartTS6Sequential6Gear().getPrice());
		
		/**** builds and tests tc120 converter ****/
		assertEquals(729d, BuilderFactory.buildPartTC120Converter().getPrice());
		
		System.out.println("----------- Configuration V2 Test / Test part price ## END -----------");
	}
	
	@Test
	@DisplayName("test exterior colors")
	public void testExteriorPartsColor() {
		
		System.out.println("----------- Configuration V2 Test / Test exterior color property values ## BEGIN -----------");
		
		/******************************** Exterior ********************************/
		
		/****** builds xc red from the factory ******/
		PartImpl xcClassicRed = BuilderFactory.buildPartXCRed();
		
		/****** asserting that the color of xc red is really red ******/
		assertEquals("red", xcClassicRed.getProperty("color").get());
		assertNotEquals("reD", xcClassicRed.getProperty("color").get());
		
		
		/****** builds xc magenta from the factory ******/
		PartImpl  xcMagenta = BuilderFactory.buildPartXCMagenta();
		 
		/****** asserting that the color of xc magenta is really magenta ******/
		assertEquals("magenta", xcMagenta.getProperty("color").get());
		assertNotEquals("Magenta", xcMagenta.getProperty("color").get());
		
		
		/****** builds xm metallic purple from the factory ******/
		PartImpl xmMetallicPurple = BuilderFactory.buildPartXMMetallicPurple();
		
		/****** asserting that the color of xm metallic purple is really purple ******/
		assertEquals("purple", xmMetallicPurple.getProperty("color").get());
		assertNotEquals("purple2", xmMetallicPurple.getProperty("color").get());
		
		System.out.println("----------- Configuration V2 Test / Test exterior color property values ## END -----------");
		
	}
	
	@Test
	@DisplayName("test interior type")
	public void testInteriorType() {
		
		/****** builds in high end from the factory ******/
		PartImpl inHghEnd = BuilderFactory.buildPartInHighEnd();
		
		/****** asserting that the type of in high end is really of high-end type ******/
		assertEquals("high-end", inHghEnd.getProperty("type").get());
		
		
		/****** builds in sport finish from the factory and asserts that it has the right type ******/
		assertEquals("finish", BuilderFactory.buildPartInSportFinish().getProperty("type").get());
	}
	
	@Test
	@DisplayName("test engine and transmission' category")
	@Tag("robustness")
	public void testEngineAndTransmissionCategory() {
		
		System.out.println("----------- Configuration V2 Test / Test engine and transmission category property values ## BEGIN -----------");
		
		/********************************** Engine *******************************************/

		/**** builds and tests eg100 gasoline 120 kw ****/
		assertEquals("gasoline", BuilderFactory.buildPartEgGasoline100KW().getProperty("category").get());
		
		/**** builds and tests eg133 gasoline 133 kw ****/
		assertEquals("gasoline", BuilderFactory.buildPartEgGasoline133KW().getProperty("category").get());
		
		/**** builds and tests ed110 diesel 110 kw ****/
		assertEquals("diesel", BuilderFactory.buildPartEgDiesel110KW().getProperty("category").get());
		
		/**** builds and tests eh120 hybrid (gasoline) 120 kw  ****/
		assertEquals("gasoline", BuilderFactory.buildPartEgHybrid120KW().getProperty("category").get());
		
		
		/************************************* Transmission *****************************************/
		
		/**** builds and tests tm5 manual 5 gears  ****/
		assertEquals("manual", BuilderFactory.buildPartTM5Manual5Gear().getProperty("category").get());
		
		/**** builds and tests tm6 manual 6 gears  ****/
		assertEquals("manual", BuilderFactory.buildPartTM6Manual6Gear().getProperty("category").get());
		
		/**** builds and tests ta5 automatic 5 gears  ****/
		assertEquals("automatic", BuilderFactory.buildPartTA5Automatic5Gear().getProperty("category").get());
		
		/**** builds and tests ts6 sequential 6 gears  ****/
		assertEquals("sequential", BuilderFactory.buildPartTS6Sequential6Gear().getProperty("category").get());
		
		/**** builds and tests tc120 converter ****/
		assertEquals("converter", BuilderFactory.buildPartTC120Converter().getProperty("category").get());
		
		System.out.println("----------- Configuration V2 Test / Test engine and transmission category property values ## END -----------");
	}
	
	@Test
	@DisplayName("test engine power")
	public void testEnginePower() {
		
		System.out.println("----------- Configuration V2 Test / Test engine power property values ## BEGIN -----------");
		
		/**** builds and tests eg100 gasoline 120 kw ****/
		assertEquals(String.valueOf(Double.valueOf("100")), BuilderFactory.buildPartEgGasoline100KW().getProperty("power").get());
		
		/**** builds and tests eg133 gasoline 133 kw ****/
		assertEquals(String.valueOf(Double.valueOf("133")), BuilderFactory.buildPartEgGasoline133KW().getProperty("power").get());
		
		/**** builds and tests ed110 diesel 110 kw ****/
		assertEquals(String.valueOf(Double.valueOf("110")), BuilderFactory.buildPartEgDiesel110KW().getProperty("power").get());
		
		/**** builds and tests eh120 hybrid (gasoline) 120 kw  ****/
		assertEquals(String.valueOf(Double.valueOf("120")), BuilderFactory.buildPartEgHybrid120KW().getProperty("power").get());
		
		System.out.println("----------- Configuration V2 Test / Test engine power property values ## END -----------");
		
	}
	
	@Test
	@DisplayName("test transmission gear")
	public void testTransmissionGear() {
		
		System.out.println("----------- Configuration V2 Test / Test transmission gears property values ## BEGIN -----------");
		
		/**** builds and tests tm5 manual 5 gears  ****/
		assertEquals(String.valueOf(Double.valueOf("5")), BuilderFactory.buildPartTM5Manual5Gear().getProperty("gear").get());
		
		/**** builds and tests tm6 manual 6 gears  ****/
		assertEquals(String.valueOf(Double.valueOf("6")), BuilderFactory.buildPartTM6Manual6Gear().getProperty("gear").get());
		
		/**** builds and tests ta5 automatic 5 gears  ****/
		assertEquals(String.valueOf(Double.valueOf("5")), BuilderFactory.buildPartTA5Automatic5Gear().getProperty("gear").get());
		
		/**** builds and tests ts6 sequential 6 gears  ****/
		assertEquals(String.valueOf(Double.valueOf("6")), BuilderFactory.buildPartTS6Sequential6Gear().getProperty("gear").get());
		
		/**** builds and tests tc120 converter ****/
		assertEquals(String.valueOf(Double.valueOf("5")), BuilderFactory.buildPartTC120Converter().getProperty("gear").get());
		
		System.out.println("----------- Configuration V2 Test / Test transmission gears property values ## END -----------");
	}
	
	@Test
	@DisplayName("test invalid properties")
	@Tag("robustness")
	public void testInvalidProperties() {
		
		System.out.println("----------- Configuration V2 Test / Test invalid properties  ## BEGIN-----------");
		
		Part eg100Gasoline100KW = BuilderFactory.buildPartEgHybrid120KW();
		Part tc120Converter = BuilderFactory.buildPartTC120Converter();
		Part inHighEnd = BuilderFactory.buildPartInHighEnd();
		Part xcMagenta = BuilderFactory.buildPartXCMagenta();
		
		/****** Engines do not have color property, should throw an IllegalArgumentException ******/
		assertThrows(IllegalArgumentException.class, () -> eg100Gasoline100KW.setProperty("color", Colors.WHITE.getColor()));
		
		/****** Engines do not have such power, i.e not among possible values, should throw IllegalArgumentException *****/
		assertThrows(IllegalArgumentException.class, () -> eg100Gasoline100KW.setProperty("power", "1000"));
		
		/****** Engines do have such property, should not throw any exception ******/
		assertDoesNotThrow(() -> eg100Gasoline100KW.setProperty("power", "100"));
		
		
		/****** Transmissions do not have power property, should throw an IllegalArgumentException ******/
		assertThrows(IllegalArgumentException.class, () -> tc120Converter.setProperty("power", "1"));
		
		/****** Transmissions do not have such gear, i.e not among possible values, should throw IllegalArgumentException *****/
		assertThrows(IllegalArgumentException.class, () -> tc120Converter.setProperty("gear", "8"));
		
		/****** Transmissions do have such gear, should not throw any exception *****/
		assertDoesNotThrow(() -> tc120Converter.setProperty("gear", "5"));
		
		
		
		/****** Interiors do not have power property, should throw an IllegalArgumentException ******/
		assertThrows(IllegalArgumentException.class, () -> inHighEnd.setProperty("power", "1"));
		
		/****** Interiors do not have red color, i.e not among possible values, should throw IllegalArgumentException *****/
		assertThrows(IllegalArgumentException.class, () -> inHighEnd.setProperty("color", Colors.ORANGE.getColor()));
		
		/****** Interiors do have such color, should not throw any exception *****/
		assertDoesNotThrow(() -> inHighEnd.setProperty("type", "standard"));
		
		
		
		/****** Exteriors do not have power property, should throw an IllegalArgumentException ******/
		assertThrows(IllegalArgumentException.class, () -> xcMagenta.setProperty("power", "133"));
		
		/****** Exteriors do not have black color, i.e not among possible values, should throw IllegalArgumentException *****/
		assertThrows(IllegalArgumentException.class, () -> xcMagenta.setProperty("color", Colors.BLACK.getColor()));
		
		/****** Exteriors do have such color, should not throw any exception *****/
		assertDoesNotThrow(() -> xcMagenta.setProperty("color", Colors.PURPLE.getColor()));
		
		System.out.println("----------- Configuration V2 Test / Test invalid properties  ## END -----------");
		
	}
	
	@Test
	@DisplayName("test configuration")
	@Tag("robustness")
    public void testConfigurationPriceAndCompleteness() {
		
		System.out.println("----------- Configuration V2 Test / Test configuration price + completeness ## BEGIN-----------");
		
		PartImpl xcRed = BuilderFactory.buildPartXCRed();
		PartImpl eg100Gasoline100KW =BuilderFactory.buildPartEgGasoline100KW();
		PartImpl eg100Hybrid = BuilderFactory.buildPartEgHybrid120KW();
		PartImpl tc120Converter = BuilderFactory.buildPartTC120Converter();
		PartImpl inSportFinish = BuilderFactory.buildPartInSportFinish(); 
		ConfigurationImpl conf = new ConfigurationImpl();
		Visitor v = new ElementVisitor();
		
		/***** adds xc red to the configuration *****/
		conf.addPart(xcRed);
		
		/**** asserts that the configuration is valid and that its price is 4000 as it is valid and contains only xc red ******/
		assertTrue(conf.isValid());
		assertEquals(4000d, conf.getPrice()); // xc red costs 4000 euros
		
		
		/**** adds eg100 to the configuration, it should still be valid but not complete*****/
		conf.addPart(eg100Gasoline100KW);
		assertTrue(conf.isValid());
		assertFalse(conf.isComplete());
		
		/**** Now the conf contains two parts, the price should be 4000 + 499 ******/
		assertEquals(4499d, conf.getPrice());
		
		/**** adds eh120 to the configuration, should make it invalid as eh120 requires tc120 *****/
		conf.addPart(eg100Hybrid);
		assertFalse(conf.isValid());
		assertFalse(conf.isComplete());
		
		
		/**** though the configuration contains 3 parts, the price should be 0 here as it is no longer valid *****/
		assertEquals(0d, conf.getPrice());
		
		/**** the html description of the configuration  should not be shown at this point *****/
		conf.accept(v);
		
		/***** adds tc120, should make the configuration valid but should remain incomplete ******/
		conf.addPart(tc120Converter);
		assertTrue(conf.isValid());
		assertFalse(conf.isComplete());
		
		/****** the price of the configuration should 4000 (xc red) + 499 (eg100) + 459 (eh120) + 729 (tc120) ****/
		assertEquals(4000 + 499 + 559 +729d, conf.getPrice());
		
		/****** adds in sport finish, should make the configuration valid and complete *****/
		conf.addPart(inSportFinish);
		assertTrue(conf.isValid());
		assertTrue(conf.isComplete());
		
		
		/***** the final price should be 4000 + 499 +559 +729 + 2875 ****/
		assertEquals(4000 + 499 + 559 + 729 + 2875d, conf.getPrice());
		
		/***** at this point the configuration is complete, the description should be available *****/
		conf.accept(v); // should print the description
		
		System.out.println("----------- Configuration V2 Test / Test configuration price + completeness ## END -----------");
		
	}
}
