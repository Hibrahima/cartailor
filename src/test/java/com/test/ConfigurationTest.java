package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import com.exceptions.ConflictingRuleException;
import com.exceptions.CrudException;
import com.helper.Utils;
import com.impl.ConfigurationImpl;
import com.impl.ConfiguratorImpl;
import com.interfaces.Configuration;
import com.interfaces.Configurator;
import com.interfaces.Part;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.Collection;

public class ConfigurationTest {
	
	private Configuration configuration;
	private Configurator configurator;
	private Part eg100Gaoline;
	private Part tm5Manual;
	private Part xcClassic;
	private Part eh120Hybrid;
	private Part tc120Normal;
	private Part ts6Sequntial;
	private Part tsf7Sequntial;
	private Part inStandard;
	
	@BeforeEach
	public void setup() throws Exception {
		configuration = new ConfigurationImpl();
		configurator = new ConfiguratorImpl();
		new BuilderFactory();
		
		Collection<Part> eg100Parts = new ArrayList<>();
		eg100Gaoline = BuilderFactory.buildPartEG100Gasoline();
		eg100Parts.add(eg100Gaoline);
		
		Collection<Part> tm5Parts = new ArrayList<>();
		tm5Manual = BuilderFactory.buildPartTM5Manual();
		tm5Parts.add(tm5Manual);
		
		Collection<Part> xcParts = new ArrayList<>();
		 xcClassic = BuilderFactory.buildPartXCClaasic();
		xcParts.add(xcClassic);
		
		Collection<Part> eh120Parts = new ArrayList<>();
		eh120Hybrid = BuilderFactory.buildPartEH120Hybrid();
		eh120Parts.add(eh120Hybrid);
		
		Collection<Part> tc120Parts = new ArrayList<>();
		 tc120Normal = BuilderFactory.buildPartTC120Normal();
		tc120Parts.add(tc120Normal);
		
		Collection<Part> ts6Parts = new ArrayList<>();
		ts6Sequntial = BuilderFactory.buildPartTS6Sequentail();
		ts6Parts.add(ts6Sequntial);
		
		Collection<Part> tsf7Parts = new ArrayList<>();
		tsf7Sequntial = BuilderFactory.buildPartTSF7Sequentail();
		tsf7Parts.add(tsf7Sequntial);
		
		Collection<Part> inParts = new ArrayList<>();
		inStandard = BuilderFactory.buildPartInStandard();
		inParts.add(inStandard);
		
		BuilderFactory.buildPartType(BuilderFactory.eg100, new ArrayList<>(), new ArrayList<>(), eg100Parts);
		BuilderFactory.buildPartType(BuilderFactory.tm5, new ArrayList<>(), new ArrayList<>(), tm5Parts);
		BuilderFactory.buildPartType(BuilderFactory.xc, BuilderFactory.xcIncompatibilities, new ArrayList<>(), xcParts);
		BuilderFactory.buildPartType(BuilderFactory.eh120, new ArrayList<>(), BuilderFactory.eh120Requiremets, eh120Parts);
		BuilderFactory.buildPartType(BuilderFactory.tc120, new ArrayList<>(), BuilderFactory.tc120Requiremets, tc120Parts);
		BuilderFactory.buildPartType(BuilderFactory.ts6, new ArrayList<>(), new ArrayList<>(), tc120Parts);
		BuilderFactory.buildPartType(BuilderFactory.tsf7, BuilderFactory.tsf7Incompatibilities, new ArrayList<>(), tsf7Parts);
		BuilderFactory.buildPartType(BuilderFactory.in, new ArrayList<>(), new ArrayList<>(), inParts);
	}
	
	@Test
	@DisplayName("test add part")
	@Tag("robustness_notify")
	public void testAddPart() throws CrudException { 
		System.out.println("----------- Configuration Test / Test add part ## BEGIN -----------");
		
		/****** trying to add a null part, should throw an exception  ****/
		assertThrows(NullPointerException.class, () -> configuration.addPart(null));
		
		/****** testing that there is no part in the configuration   ****/
		assertEquals(0, Utils.convertIteratorToList(configuration.getParts()).size());
		
		/****** testing that the newly created part is not null ******/
		assertNotNull(eg100Gaoline);
		
		/****** testing the part eg100Gaoline is well associated to its part type eg100 ******/
		assertEquals(BuilderFactory.eg100, eg100Gaoline.getPartType());
		
		/****** Mocking and testing ******/
		Configurator conf1 = mock(Configurator.class); 
		Configurator conf2 = mock(Configurator.class);
		
		/****** registering both configurators ******/
		configuration.register(conf1);
		configuration.register(conf2);
		
		/****** adding a part to the configuration, should notify both observers ******/
		configuration.addPart(eg100Gaoline);
		
		/****** testing that eg100Gaoline has been added to the configuration ******/
		assertTrue(Utils.convertIteratorToList(configuration.getParts()).contains(eg100Gaoline));
		assertEquals(1, Utils.convertIteratorToList(configuration.getParts()).size());
		
		/****** verify that updates have been sent to both observers ******/
		verify(conf1).update(configuration);
		verify(conf2).update(configuration);
		
		/****** testing that the configurator has the correct configuration ******/
		configurator.update(configuration);
		assertEquals(configuration, configurator.getConfiguration());
		
		/****** trying to add eg100Gasoline a second time ******/
		assertThrows(IllegalArgumentException.class, () ->configuration.addPart(eg100Gaoline));
		
		/****** adding tm5 to the configuration ******/
		configuration.addPart(tm5Manual);
		assertEquals(BuilderFactory.tm5, tm5Manual.getPartType());
		assertTrue(Utils.convertIteratorToList(configuration.getParts()).contains(tm5Manual));
		assertEquals(2, Utils.convertIteratorToList(configuration.getParts()).size());
		
		System.out.println("----------- Configuration Test / Test add part ## END -----------");
		
	}
	
	@Test
	@DisplayName("test remove part")
	@Tag("robustness_notify")
	public void testRemovePart() throws CrudException {
		System.out.println("----------- Configuration Test / Test remove part ## BEGIN-----------");
		
		assertThrows(NullPointerException.class, () -> configuration.removePart(null));
		assertEquals(0, Utils.convertIteratorToList(configuration.getParts()).size());
		
		/****** asserting the nullness and part type reference of xcClassic ******/
		assertNotNull(xcClassic);
		assertEquals(BuilderFactory.xc, xcClassic.getPartType());
		
		/****** trying to remove a part which is not added to the configuration yet ******/
		assertThrows(IllegalArgumentException.class, () -> configuration.removePart(xcClassic));
		
		/****** adding xcClassic into the configuration ******/
		configuration.addPart(xcClassic);
		assertTrue(Utils.convertIteratorToList(configuration.getParts()).contains(xcClassic));
		assertEquals(1, Utils.convertIteratorToList(configuration.getParts()).size());
		
		
		/****** Mocking and testing ******/
		Configurator conf1 = mock(Configurator.class);
		Configurator conf2 = mock(Configurator.class);
		
		/****** registering both configurators ******/
		configuration.register(conf1);
		configuration.register(conf2);
		
		/****** removing a part, should notify both observers ******/
		configuration.removePart(xcClassic);
		
		/****** testing that the previously removed part has been removed properly ******/
		assertFalse(Utils.convertIteratorToList(configuration.getParts()).contains(xcClassic));
		assertEquals(0, Utils.convertIteratorToList(configuration.getParts()).size());	
		
		/****** verify that updates have been sent to both observers ******/
		verify(conf1).update(configuration);
		verify(conf2).update(configuration);
		
		/****** testing that the configurator has the correct configuration ******/
		configurator.update(configuration);
		assertEquals(configuration, configurator.getConfiguration());
		
		System.out.println("----------- Configuration Test / Test remove part ## END -----------");
		
	}
	
	@Test
	@DisplayName("test  register")
	@Tag("robustness")
	public void testRegister() {
		System.out.println("----------- Configuration Test / Test register observer ## BEGIN -----------");
		
		/****** creating an observer ******/
		Configurator conf = new ConfiguratorImpl();
		
		/****** registering that observer ******/
		configuration.register(conf);
		assertTrue(Utils.convertIteratorToList(configuration.getRegisteredObservers()).contains(conf));
		assertTrue(configuration.isRegistered(conf));
		
		/****** trying to add the same configurator a second time, should throw an exception ******/
		assertThrows(IllegalArgumentException.class, () -> configuration.register(conf));
		assertEquals(1, Utils.convertIteratorToList(configuration.getRegisteredObservers()).size());
		
		System.out.println("----------- Configuration Test / Test register observer ## END -----------");
	}
	
	@Test
	@DisplayName("test unregister")
	@Tag("robustness")
	public void testUnegister() {
		System.out.println("----------- Configuration Test / Test unregister observer ## BEGIN -----------");
		
		/****** creating an observer ******/
		Configurator conf = new ConfiguratorImpl();
		
		/****** registering that observer ******/
		configuration.register(conf);
		
		/****** testing that the observer has been registered ******/
		assertTrue(Utils.convertIteratorToList(configuration.getRegisteredObservers()).contains(conf));
		assertTrue(configuration.isRegistered(conf));
		
		/****** unregistering the previously registered observer ******/
		configuration.unregister(conf);
		
		/****** asserting that the observer has been unregistered properly ******/
		assertFalse(Utils.convertIteratorToList(configuration.getRegisteredObservers()).contains(conf));
		assertEquals(0, Utils.convertIteratorToList(configuration.getRegisteredObservers()).size());
		
		/****** trying to remove an observer which is not registered, should throw an exception ******/
		assertThrows(IllegalArgumentException.class, () -> configuration.unregister(conf));
		
		System.out.println("----------- Configuration Test / Test unregister observer ## END -----------");
	}
	
	@Test
	@DisplayName("is valid")
	@Tag("robustness")
	public void testIsValid() throws ConflictingRuleException, CrudException {
		System.out.println("----------- Configuration Test / Test is valid ## BEGIN -----------");
		
		/****** adding eg100, eg100 has no incompatible neither required parts /*****/
		configuration.addPart(eg100Gaoline); 
		
		/****** adding tm5, tm5 has no incompatible neither required parts ******/
		configuration.addPart(tm5Manual); 
		
		/****** adding xc, xc has no required parts but does have an incompatible part (EG210 *****/
		/***** at this point EG200 is not part of the configuration yet *****/
		configuration.addPart(xcClassic); 
		
		/***** trying to see if the configuration is valid or not, should be valid at this point *****/
		assertTrue(configuration.isValid());
		
		
		/***** adding ts6, ts6 has no incompatible neither required parts, hence should not invalid the configuration *****/
		configuration.addPart(ts6Sequntial);
		
		/***** trying to see if the configuration is valid or not, should still be valid at this point *****/
		assertTrue(configuration.isValid());
		
		/***** adding eh120, eh120 does requires tc120, hence the configuration should be invalid unless tc120 is added at a later time *****/
		configuration.addPart(eh120Hybrid);
		
		/***** trying to see if the configuration is valid or not, should not be valid at this point *****/
		assertFalse(configuration.isValid());
		
		/***** adding tc120 which requires eh120 which is already present in the configuration *****/
		/***** this should to make the configuration valid again *****/
		 configuration.addPart(tc120Normal);
		 assertTrue(configuration.isValid());
		 
		 /***** adding tsf7 into the configuration, tsf4 is incompatible with eg100 which is already in the configuration *****/
		 /***** this should make the configuration invalid *****/
		configuration.addPart(tsf7Sequntial);
		assertFalse(configuration.isValid());
		
		/***** removing the only part (eg100) that makes the configuration invalid *****/
		configuration.removePart(eg100Gaoline);
		assertTrue(configuration.isValid());
		
		assertEquals(6, Utils.convertIteratorToList(configuration.getParts()).size());
		
		System.out.println("----------- Configuration Test / Test is valid ## END -----------");
			
	}
	
	@Test
	@Tag("robustness")
	@DisplayName("is complete")
	public void testIsComplete() throws ConflictingRuleException, CrudException {
		System.out.println("----------- Configuration Test / Test is complete ## BEGIN -----------");
		
		/***** adding eg100, eg100 has no incompatible neither required parts *****/
		configuration.addPart(eg100Gaoline);
		
		/***** adding tm5, tm5 has no incompatible neither required parts *****/
		configuration.addPart(tm5Manual);
		
		/***** trying to see if the configuration is valid or not, should be valid at this point *****/
		assertTrue(configuration.isValid());
		
		/***** at this point the configuration is valid but should not be complete because only 2 categories are present *****/
		assertFalse(configuration.isComplete());
		
		/***** adding xc, xc has no required parts but does have an incompatible part (EG210) which is not part of the configuration yet *****/
		configuration.addPart(xcClassic); 
		assertFalse(configuration.isComplete());
		
		/***** adding a part (inStandard) for the only category (Interior) missing to make this configuration complete *****/
		configuration.addPart(inStandard);
		assertTrue(configuration.isComplete());
		
		
		/***** adding ts6Sequential into the configuration *****/ 
		/***** ts6 has no incompatible neither required parts, hence should not invalid the configuration neither make it incomplete *****/
		configuration.addPart(ts6Sequntial);
		assertTrue(configuration.isComplete());
		
		/***** at this point we do have a complete configuration with 2 parts from the same category (Transmission) *****/
		/***** let's remove one of these 2 parts, removing tm5 from the configuration, the configuration should still remain complete *****/
		configuration.removePart(tm5Manual);
		assertTrue(configuration.isComplete());
		
		/***** adding eh120, eh120 does require tc120, hence the configuration should be incomplete since it is no longer valid unless tc120 is added at a later time *****/
		configuration.addPart(eh120Hybrid); 
		assertFalse(configuration.isComplete());
		
		/***** adding tc120 which requires eh120 which is already present in the configuration *****/
		/***** this should make the configuration valid and complete again *****/
		 configuration.addPart(tc120Normal);
		 assertTrue(configuration.isComplete());
		 
		/***** adding tsf7 into the configuration, tsf4 is incompatible with eg100 which is already in the configuration *****/
		/***** this should make the configuration invalid and incomplete *****/
		configuration.addPart(tsf7Sequntial);
		assertFalse(configuration.isComplete());
		
		/***** removing the only part (eg100) that makes the configuration invalid *****/
		configuration.removePart(eg100Gaoline);
		assertTrue(configuration.isComplete());
		assertEquals(6, Utils.convertIteratorToList(configuration.getParts()).size());
		
		System.out.println("----------- Configuration Test / Test is complete ## END -----------");
			
	}
	
	@Test
	@Tag("robustness")
	@DisplayName("test null")
	public void testNull() {
		System.out.println("----------- Configuration Test / Test null objects ## BEGIN-----------");
		
		assertThrows(NullPointerException.class, () -> configuration.addPart(null));
		assertThrows(NullPointerException.class, () -> configuration.removePart(null));
		assertThrows(NullPointerException.class, () -> configuration.register(null));
		assertThrows(NullPointerException.class, () -> configuration.unregister(null));
		assertThrows(NullPointerException.class, () -> configuration.getIncompatibleParts(null));
		assertThrows(NullPointerException.class, () -> configuration.getRequiredParts(null));
		assertThrows(NullPointerException.class, () -> configuration.isRegistered(null));
		
		System.out.println("----------- Configuration Test / Test null objects## END  -----------");
	}
	

}
