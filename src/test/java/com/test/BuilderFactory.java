package com.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import com.exceptions.ConflictingRuleException;
import com.exceptions.CrudException;
import com.helper.Utils;
import com.impl.Categories;
import com.impl.PartImpl;
import com.impl.PartTypeImpl;
import com.interfaces.Part;
import com.interfaces.PartType;
import com.internals.Engine;
import com.internals.Exterior;
import com.internals.Interior;
import com.internals.Transmission;

public class BuilderFactory {

	public static class EG100 extends PartImpl {

	};

	public static class EG133 extends PartImpl {

	};

	public static class EG210 extends PartImpl {

	};

	public static class ED110 extends PartImpl {

	};

	public static class ED180 extends PartImpl {

	};

	public static class EH120 extends PartImpl {

	};

	public static class TM5 extends PartImpl {

	};

	public static class TM6 extends PartImpl {

	};

	public static class TA5 extends PartImpl {

	};

	public static class TS6 extends PartImpl {

	};

	public static class TSF7 extends PartImpl {

	};

	public static class TC120 extends PartImpl {

	};

	public static class XC extends PartImpl {

	};

	public static class XM extends PartImpl {

	};

	public static class XS extends PartImpl {

	};

	public static class IN extends PartImpl {

	};

	public static class IH extends PartImpl {

	};

	public static class IS extends PartImpl {

	};

	/******************Part types engine*****************************/
	public static PartTypeImpl eg100;
	public static PartTypeImpl eg133;
	public static PartTypeImpl eg210;
	public static PartTypeImpl ed110;
	public static PartTypeImpl ed180;
	public static PartTypeImpl eh120;

	/*******************Part types transmission*********************/
	public static PartTypeImpl tm5;
	public static PartTypeImpl tm6;
	public static PartTypeImpl ta5;
	public static PartTypeImpl ts6;
	public static PartTypeImpl tsf7;
	public static PartTypeImpl tc120;

	/********************Part types exterior**************************/
	public static PartTypeImpl xc;
	public static PartTypeImpl xm;
	public static PartTypeImpl xs;

	/********************Part types interior*************************/
	public static PartTypeImpl in;
	public static PartTypeImpl ih;
	public static PartTypeImpl is;

	
	/*********************Parts engine******************************/
	public static Part eg100Gasoline;
	public static Part ed110Diesel;
	public static Part eh120Hybrid;

	/********************Parts transmission*************************/
	public static Part tm5Manual;
	public static Part ta5Automatic;
	public static Part ts6Sequential;
	public static Part tsf7Sequential;
	public static Part tc120Normal;

	/*******************Parts exterior*****************************/
	public static Part xcClassic;
	public static Part xmMetallic;
	public static Part xsRed;

	/*****************Parts interior*******************************/
	public static Part inStandard;
	public static Part ihHighEnd;
	public static Part isSport;
	
	/******************Requirements*****************/
	public static List<PartType> eh120Requiremets;
	public static List<PartType> tc120Requiremets;
	public static List<PartType> xsRequiremets;
	public static List<PartType> isRequiremets;
	
	/******************Incompatibilities************/
	public static List<PartType> ta5Incompatibilities;
	public static List<PartType> tsf7Incompatibilities;
	public static List<PartType> xcIncompatibilities;
	public static List<PartType> xmIncompatibilities;
	public static Collection<PartType> xsIncompatibilities;
	public static List<PartType> isIncompatibilities;
	
	/********* list of parts for each part type ************/
	
	

	
	
	/**
	 * 
	 * @throws Exception if an exception occurs while instantiating part types
	 */
	public BuilderFactory() throws Exception {
		eg100 = new PartTypeImpl("EG100", "Gasoline, 100 kW", Categories.ENGINE, EG100.class);
		eg133 = new PartTypeImpl("EG133", "Gaoline, 133 kW", Categories.ENGINE, EG133.class);
		eg210 = new PartTypeImpl("EG210", "Gaoline, 210 kW", Categories.ENGINE, EG210.class);
		ed110 = new PartTypeImpl("ED110", "Diesel, 110 kW", Categories.ENGINE, ED110.class);
		ed180 = new PartTypeImpl("ED180", "Diesel, 180 kW", Categories.ENGINE, ED180.class);
		eh120 = new PartTypeImpl("EH120", "Gaoline/electric hybrid, 120 kW", Categories.ENGINE, EH120.class);
		

		tm5 = new PartTypeImpl("TM5", "Manual, 5 gears", Categories.TRANSMISSION, TM5.class);
		tm6 = new PartTypeImpl("TM6", "Manual, 6 gears", Categories.TRANSMISSION, TM6.class);
		ta5 = new PartTypeImpl("TA5", "Automatic, 5 gears", Categories.TRANSMISSION, TA5.class);
		ts6 = new PartTypeImpl("TS6", "Sequential, 6 gears", Categories.TRANSMISSION, TS6.class);
		tsf7 = new PartTypeImpl("TSF7", "Sequential. 7 gears, 4 wheels drive", Categories.TRANSMISSION, TSF7.class);
		tc120 = new PartTypeImpl("TC120", "Converter, 120 kW max", Categories.TRANSMISSION, TC120.class);


		xc = new PartTypeImpl("XC", "Classic paint", Categories.EXTERIOR, XC.class);
		xm = new PartTypeImpl("XM", "Metallic paint", Categories.EXTERIOR, XM.class);
		xs = new PartTypeImpl("XS", "Red paint and sport decoration", Categories.EXTERIOR, XS.class);

		
		in = new PartTypeImpl("IN", "Standard interior", Categories.INTERIOR, IN.class);
		ih = new PartTypeImpl("IH", "High-end interior", Categories.INTERIOR, IH.class);
		is = new PartTypeImpl("IS", "Sport finish", Categories.INTERIOR, IS.class);
		
		
		eg100Gasoline = eg100.newInstance();
		ed110Diesel = eg210.newInstance();
		eh120Hybrid = eh120.newInstance();
		
		tm5Manual = tm5.newInstance();
		ta5Automatic = ta5.newInstance();
		ts6Sequential = ts6.newInstance();
		tsf7Sequential = tsf7.newInstance();
		tc120Normal = tc120.newInstance();
		
		xcClassic = xc.newInstance();
		xmMetallic = xm.newInstance();
		xsRed = xs.newInstance();
		
		inStandard = in.newInstance();
		ihHighEnd = ih.newInstance();
		isSport = is.newInstance();
		
		eh120Requiremets = new ArrayList<>();
		eh120Requiremets.add(tc120);
		tc120Requiremets = new ArrayList<>();
		tc120Requiremets.add(eh120);
		xsRequiremets = new ArrayList<>();
		xsRequiremets.add(is);
		isRequiremets = new ArrayList<>();
		isRequiremets.add(xs);
		
		ta5Incompatibilities = new ArrayList<>();
		ta5Incompatibilities.add(eg100);
		tsf7Incompatibilities = new ArrayList<>();
		tsf7Incompatibilities.add(eg100); 
		tsf7Incompatibilities.add(eg133);
		tsf7Incompatibilities.add(ed110);
		xcIncompatibilities = new ArrayList<>();
		xcIncompatibilities.add(eg210);
		xmIncompatibilities = new ArrayList<>();
		xmIncompatibilities.add(eg100);
		xsIncompatibilities = new ArrayList<>();
		xsIncompatibilities.add(eg100);
		isIncompatibilities = new ArrayList<>();
		isIncompatibilities.add(eg100);
		isIncompatibilities.add(tm5);
		
	}

	/**
	 * Builds a particular part type with all its parts, incompatible and required parts as well
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @param pt the part type to build
	 * @param incompatibilities the list of incompatible parts to add to pt
	 * @param requirements the list of required parts to add to pt
	 * @param parts the list of parts to add to pt
	 * @throws NullPointerException if pt is null
	 * @throws NullPointerException if incompatibilities is null
	 * @throws NullPointerException if requirements is null
	 * @throws ConflictingRuleException if a conflict occurs
	 * @throws CrudException if an exception of this type is thrown
	 * @return the constructed part type
	 */
	public static PartType buildPartType(PartType pt, Collection<PartType> incompatibilities, Collection<PartType> requirements, 
	 Collection<Part> parts) throws ConflictingRuleException, CrudException{
		Objects.requireNonNull(pt, "part type cannot be null");
		Objects.requireNonNull(incompatibilities, "incompatibility list cannot be null");
		Objects.requireNonNull(requirements, "requirement list cannot be null");
		Objects.requireNonNull(parts, "parts cannot be null");
		pt.addIncompatibilities(Utils.getIterator(incompatibilities));
		pt.addRequirements(Utils.getIterator(requirements));
		for(Part p : parts) {
			pt.addPart(p);
		}
		return pt;
	}
	
	/**
	 * Builds eg100 gasoline and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartEG100Gasoline()  {
		eg100Gasoline.setPartType(eg100);
		eg100Gasoline.setName(eg100.getName()+" Gasoline");
		return eg100Gasoline;
	}
	
	/**
	 * Builds tm5 automatic and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartTM5Manual() {
		tm5Manual.setPartType(tm5);
		tm5Manual.setName(tm5.getName()+" Manual");
		return tm5Manual;
	}
	
	/**
	 * Builds xc classic and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartXCClaasic() {
		xcClassic.setPartType(xc);
		xcClassic.setName(xc.getName()+" Classic");
		return xcClassic;
	}
	
	/**
	 * Builds ta5 automatic and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartTA5Automatic() {
		ta5Automatic.setPartType(ta5);
		ta5Automatic.setName(ta5.getName()+" Automatic");
		return ta5Automatic;
	}
	
	/**
	 * Builds xm metallic and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartXMMetallic() {
		xmMetallic.setPartType(xm);
		xmMetallic.setName(xm.getName()+" Metallic");
		return xmMetallic;
	}
	
	/**
	 * Builds ts6 sequential and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartTS6Sequentail() {
		ts6Sequential.setPartType(ts6);
		ts6Sequential.setName(ts6.getName()+" Sequential");
		return ts6Sequential;
	}
	
	/**
	 * Builds tsf7 sequential and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartTSF7Sequentail() {
		tsf7Sequential.setPartType(tsf7);
		tsf7Sequential.setName(tsf7.getName()+" Sequential");
		return tsf7Sequential;
	}
	
	/**
	 * Builds is sport and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartISSport() {
		isSport.setPartType(is);
		isSport.setName(is.getName()+" Sport");
		return isSport;
	}
	
	/**
	 * Builds eh120 hybrid and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartEH120Hybrid() {
		eh120Hybrid.setPartType(eh120);
		eh120Hybrid.setName(eh120.getName()+" Hybrid");
		return eh120Hybrid;
	}
	
	/**
	 * Builds tc120 normal and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartTC120Normal()  {
		tc120Normal.setPartType(tc120);
		tc120Normal.setName(tc120.getName()+" Normal");
		return tc120Normal;
	}
	
	/**
	 * Builds in standard and sets its part type and name
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static Part buildPartInStandard() {
		inStandard.setPartType(in);
		inStandard.setName(in.getName()+" Standard");
		return inStandard;
	}
	
	
	/************************************* For V2 ********************************************/
	
	/************************************* Begin Exterior ***********************************/
	
	/**
	 * Builds xc classic red and sets its part type, description, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartXCRed() {
		class xc extends Exterior{
			
			public xc() {
				setProperty("color", "red");
			}
		}
		xc xcRed = new xc();
		xcRed.setName("xc red");
		xcRed.setPartType(xc);
		return xcRed;
	}
	
	
	/**
	 * Builds xc classic red and sets its part type, description, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartXCMagenta() {
		class xc extends Exterior{
			
			public xc() {
				setProperty("color", "magenta");
			}
			
			@Override
			public String getDescription() {
				return "The classic exterior magenta style is one of the most loved part types especially for young women";
			}
		}
		xc xcMagenta = new xc();
		xcMagenta.setName("xc red");
		xcMagenta.setPartType(xc);
		return xcMagenta;
	}
	
	/**
	 * Builds xm metallic purple and sets its part type, description, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartXMMetallicPurple() {
		class xm extends Exterior{
			
			public xm() {
				setProperty("color", "purple");
			}	
			
			@Override
			public double getPrice() {
				return 799d;
			}
		}
		xm xmPuple = new xm();
		xmPuple.setName("xm metallic purple");
		xmPuple.setPartType(xm);
		return xmPuple;
	}
	
	/***************************************** End Exterior ************************************/
	
	/***************************************** Begin Interior ***********************************/
	
	/**
	 * Builds xm metallic purple and sets its part type, description, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartInHighEnd() {
		class in extends Interior{
			
			public in() {
				setProperty("type", "high-end");
			}	
			
		}
		in inBlack = new in();
		inBlack.setName("In Standard Black - very chic");
		inBlack.setPartType(in);
		return inBlack;
	}
	
	/**
	 * Builds in sport yellow and sets its part type, description, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartInSportFinish() {
		class in extends Interior{
			
			public in() {
				setProperty("type", "finish");
			}	
			
		}
		in inSport = new in();
		inSport.setName("In Sport Finish - very chic");
		inSport.setPartType(in);
		return inSport;
	}
	
	
	/************************************** End Interior *****************************************/
	
	
	/************************************** Begin Engine ****************************************/
	
	/**
	 * Builds eg100 gasoline 120 kw and sets its part type, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartEgGasoline100KW() {
		class eg100KW extends Engine{
			
		}
		eg100KW toReturn = new eg100KW();
		toReturn.setName("EG100 Gasoline 120 Kw Engine");
		toReturn.setPartType(eg100);
		return toReturn;
	}
	
	/**
	 * Builds eg133 gasoline 133 kw and sets its part type, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartEgGasoline133KW() {
		class eg133KW extends Engine{
			
			public eg133KW() {
				setProperty("power", "133");
			}
		}
		eg133KW toReturn = new eg133KW();
		toReturn.setName("EG133 Gasoline 133 Kw Engine");
		toReturn.setPartType(eg133);
		return toReturn;
	}
	
	/**
	 * Builds ed110 diesel 110 kw and sets its part type, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartEgDiesel110KW() {
		class ed110KW extends Engine{
			
			public ed110KW() {
				setProperty("power", "110");
				setProperty("category", "diesel");
			}
		}
		ed110KW toReturn = new ed110KW();
		toReturn.setName("ED110 Diesel 110 KW Engine");
		toReturn.setPartType(ed110);
		return toReturn;
	}
	
	
	/**
	 * Builds eh120 hybrid (gasoline) 120 kw and sets its part type, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartEgHybrid120KW() {
		class eh120KW extends Engine{
			
			public eh120KW() {
				setProperty("power", "120");
			}
		}
		eh120KW toReturn = new eh120KW();
		toReturn.setName("EH120 Hybrid Diesel 110 KW Engine");
		toReturn.setPartType(eh120);
		return toReturn;
	}
	
	
	/*************************************** End Engine ********************************/
	
	/*************************************** Begin Transmission ***********************/
	
	/**
	 * Builds tm5 manual 5 gears  and sets its part type, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartTM5Manual5Gear() {
		class tm5Manual extends Transmission{
		
		}
		tm5Manual toReturn = new tm5Manual();
		toReturn.setName("TM5 Manual 5 Gears Transmission");
		toReturn.setPartType(tm5);
		return toReturn;
	}
	
	/**
	 * Builds tm6 manual 6 gears  and sets its part type, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartTM6Manual6Gear() {
		class tm6Manual extends Transmission{
		
			public tm6Manual() {
				setProperty("gear", "6");
			}
		}
		tm6Manual toReturn = new tm6Manual();
		toReturn.setName("TM6 Manual 6 Gears Transmission");
		toReturn.setPartType(tm6);
		return toReturn;
	}
	
	/**
	 * Builds ta5 automatic 5 gears  and sets its part type, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartTA5Automatic5Gear() {
		class ta5Automatic extends Transmission{
		
			public ta5Automatic() {
				setProperty("category", "automatic");
			}
		}
		ta5Automatic toReturn = new ta5Automatic();
		toReturn.setName("TA5 Automatic 5 Gears Transmission");
		toReturn.setPartType(ta5);
		return toReturn;
	}
	
	
	/**
	 * Builds ts6 sequential 6 gears  and sets its part type, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartTS6Sequential6Gear() {
		class ts6Sequential extends Transmission{
		
			public ts6Sequential() {
				setProperty("category", "sequential");
				setProperty("gear", "6");
			}
		}
		ts6Sequential toReturn = new ts6Sequential();
		toReturn.setName("TS6 Sequential 6 Gears Transmission");
		toReturn.setPartType(ts6);
		return toReturn;
	}
	
	
	/**
	 * Builds tc120 converter and sets its part type, name and properties
	 * A part is associated to only and only one part type
	 * A part type may have multiple parts
	 * 
	 * @return the constructed part
	 */
	public static PartImpl buildPartTC120Converter() {
		class tc120Converter extends Transmission{
		
			public tc120Converter() {
				setProperty("category", "converter");
			}
			
			@Override
			public double getPrice() {
				return 729d;
			}
		}
		
		tc120Converter toReturn = new tc120Converter();
		toReturn.setName("TC120 Converter 5 Gears Transmission");
		toReturn.setPartType(tc120);
		return toReturn;
	}
	
	/*************************************** End Transmission *********************************/
	
}
