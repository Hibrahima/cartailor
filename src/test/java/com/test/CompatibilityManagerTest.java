package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import com.exceptions.ConflictingRuleException;
import com.exceptions.CrudException;
import com.helper.Utils;
import com.impl.CompatibilityManagerImpl;
import com.interfaces.CompatibilityManager;
import com.interfaces.PartType;

public class CompatibilityManagerTest {

	private CompatibilityManager manager;

	@BeforeEach
	public void setup() throws Exception {
		manager = new CompatibilityManagerImpl();
		new BuilderFactory();
	}

	@Test
	@DisplayName("nullness and size checking")
	@Tag("robustness")
	public void testListSizeAndNullness() {
		System.out.println("----------- Compatibility Manager Test / Test empty part types' incompatibility and requirement list ## BEGIN-----------");
		
		/****** at this point nothing is added to tsf7, the instance object tsf7 is not fully constructed yet ******/
		List<PartType> tsf7Incompatibilities = Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.tsf7));
		List<PartType> tsf7Requirements = Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.tsf7));

		/****** asserting that they are not null and are empty at the very beginning ******/
		assertNotNull(manager.getIncompatibleParts(BuilderFactory.tsf7));
		assertNotNull(manager.getRequiredParts(BuilderFactory.tsf7));
		assertEquals(0, tsf7Incompatibilities.size());
		assertEquals(0, tsf7Requirements.size());
		
		System.out.println("----------- Compatibility Manager Test / Test empty part types' incompatibility and requirement list ## END-----------");
	}

	@Test
	@DisplayName("add incompatibilities")
	@Tag("robustness")
	public void testAddSameIncompatiblePartTypeTwice() {
		System.out.println("----------- Compatibility Manager Test / Test add same imcompatible part to a given part type ## BEGIN -----------");
		
		/****** at this point nothing is added to tsf7, the instance object tsf7 is not fully constructed yet ******/
		List<PartType> tsf7Incompatibilities = Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.tsf7));

		/******adding eg133 twice to a collection that will be used for setting tsf7' incompatibulity list ******/
		/****** this should throw an exception ******/
		tsf7Incompatibilities.add(BuilderFactory.tsf7Incompatibilities.get(1));
		tsf7Incompatibilities.add(BuilderFactory.tsf7Incompatibilities.get(1)); // add eg133
		assertThrows(CrudException.class,
				() -> manager.addIncompatibilities(BuilderFactory.tsf7, Utils.getIterator(tsf7Incompatibilities)));
		
		/****** asserting that only one of them has been added ******/
		assertTrue(Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.tsf7)).contains(BuilderFactory.eg133));
		assertEquals(1, Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.tsf7)).size());

		System.out.println("----------- Compatibility Manager Test / Test add same imcompatible part to a given part type ## END -----------");
	}

	@Test
	@Tag("robustness")
	@DisplayName("adding a part to itself as incompatible")
	public void testAddItSelfAsIncompatible() {
		System.out.println("----------- Compatibility Manager Test / Test add the reference part type as incompatible with itself ## BEGIN -----------");
		
		List<PartType> tsf7Incompatibilities = Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.tsf7));
		
		/****** trying to set tsf7 as incompatible with itself, should also throw an exception ******/
		tsf7Incompatibilities.add(BuilderFactory.tsf7);
		assertThrows(CrudException.class,
				() -> manager.addIncompatibilities(BuilderFactory.tsf7, Utils.getIterator(tsf7Incompatibilities)));
		
		System.out.println("----------- Compatibility Manager Test / Test add the reference part type as incompatible with itself ## END-----------");
	}

	@Test
	@DisplayName("add incompatible part types which are required")
	@Tag("robustness")
	public void testAddIncompatibilePartWhichIsRequired() throws ConflictingRuleException, CrudException {
		System.out.println("----------- Compatibility Manager Test / Test add incompatible part types which are required ## BEGIN -----------");
		
		List<PartType> tsf7Requirements = Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.tsf7));

		/****** trying to add a part type as incompatible while that type is required ******/
		tsf7Requirements.add(BuilderFactory.tsf7Incompatibilities.get(2));
		
		/****** adding ed110 as a required part type for tsf7 ******/
		manager.addRequirements(BuilderFactory.tsf7, Utils.getIterator(tsf7Requirements)); 
		
		/****** adding tsf7 to ed110' incompatibilities, should throw a ConflictingRuleException ******/
		List<PartType> temp = new ArrayList<>();
		temp.add(BuilderFactory.tsf7);
		assertThrows(ConflictingRuleException.class, () -> manager.addIncompatibilities(BuilderFactory.ed110, Utils.getIterator(temp)));
				
		
		/******  trying to add ed110 as incompatible with tsf7, should throw an exception ******/
		assertThrows(ConflictingRuleException.class,
				() -> manager.addIncompatibilities(BuilderFactory.tsf7, Utils.getIterator(tsf7Requirements)));
		
		/****** asserting that ed110 has not been added as there was a conflict ******/
		assertFalse(Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.tsf7))
				.contains(BuilderFactory.ed110));
		
		System.out.println("----------- Compatibility Manager Test / Test add incompatible part types which are required ## END -----------");
	}
	
	@Test
	@Tag("robustness")
	@DisplayName("test null")
	public void testNull() {
		System.out.println("----------- Compatibility Manager Test / Test null objects ## BEGIN -----------");
		
		assertThrows(NullPointerException.class, () -> manager.getIncompatibleParts(null));
		assertThrows(NullPointerException.class, () -> manager.getRequiredParts(null));
		assertThrows(NullPointerException.class, () -> manager.addIncompatibilities(null, null));
		assertThrows(NullPointerException.class, () -> manager.addRequirements(null, null));
		assertThrows(NullPointerException.class, () -> manager.removeIncompatibilities(null, null));
		assertThrows(NullPointerException.class, () -> manager.removeRequirements(null, null));
		
		System.out.println("----------- Compatibility Manager Test / Test null objects ## END -----------");
	}
	
	@Test
	@Tag("robustness")
	@DisplayName("add same part type as required twice")
	public void testAddSameRequiredPartTwice() {
		System.out.println("----------- Compatibility Manager Test / Test add same required part type twice ## BEGIN -----------");
		
		List<PartType> isRequirements = Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.is));
		assertEquals(0, isRequirements.size());

		/****** trying to add the same part type again, should throw an exception ******/
		isRequirements.add(BuilderFactory.isRequiremets.get(0)); //add xs
		isRequirements.add(BuilderFactory.isRequiremets.get(0)); //add xs
		assertThrows(CrudException.class,
				() -> manager.addRequirements(BuilderFactory.is, Utils.getIterator(isRequirements)));
		
		/****** asserting that only one of them has been added ******/
		assertTrue(Utils.convertIteratorToList(manager.getRequiredParts(BuilderFactory.is)).contains(BuilderFactory.xs));
		assertEquals(1, Utils.convertIteratorToList(manager.getRequiredParts(BuilderFactory.is)).size());
		
		System.out.println("----------- Compatibility Manager Test / Test add same required part type twice ## END -----------");
	}
	
	@Test
	@Tag("robustness")
	@DisplayName("add requirement with itself")
	public void testAddItSelfAsRequired() {
		System.out.println("----------- Compatibility Manager Test / Test add same part type as required with itself ## BEGIN -----------");
		
		List<PartType> isRequirements = Utils.convertIteratorToList(manager.getRequiredParts(BuilderFactory.is));
		
		/****** trying to set is as required with itself, should also throw an exception ******/
		isRequirements.add(BuilderFactory.is);
		assertThrows(CrudException.class,
				() -> manager.addRequirements(BuilderFactory.is, Utils.getIterator(isRequirements)));
		
		System.out.println("----------- Compatibility Manager Test / Test add same part type as required with itself ## END -----------");
	}
	
	@Test
	@DisplayName("add required part types that are incompatible")
	@Tag("robustness")
	public void testAddRequiredPartWhichIsIncompatible() throws ConflictingRuleException, CrudException {
		System.out.println("----------- Compatibility Manager Test / Test add required part types thar are  incompatible ## BEGIN-----------");
		
		List<PartType> isIncompatibilities = Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.is));

		/****** trying to add a part type as required while that type is incompatible ******/
		isIncompatibilities.add(BuilderFactory.isIncompatibilities.get(1)); 
		manager.addIncompatibilities(BuilderFactory.is, Utils.getIterator(isIncompatibilities)); //add tm5 as incompatible
		
		
		/****** adding is to tm5' incompatibilities, should throw a ConflictingRuleException ******/
		List<PartType> temp = new ArrayList<>();
		temp.add(BuilderFactory.is);
		assertThrows(ConflictingRuleException.class, () -> manager.addRequirements(BuilderFactory.tm5, Utils.getIterator(temp)));
		assertThrows(ConflictingRuleException.class, () -> manager.addRequirements(BuilderFactory.is, Utils.getIterator(isIncompatibilities)));
		
		/****** trying to add tm5 as required for is ******/
		assertThrows(ConflictingRuleException.class,
				() -> manager.addRequirements(BuilderFactory.is, Utils.getIterator(isIncompatibilities)));
		
		assertFalse(Utils.convertIteratorToList(manager.getRequiredParts(BuilderFactory.is)).contains(BuilderFactory.tm5));
		
		System.out.println("----------- Compatibility Manager Test / Test add required part types thar are  incompatible ## END-----------");
		
	}
	
	@Test
	@Tag("robustness")
	@DisplayName("remove incpmpatibilities")
	public void testRemoveIncompatibilities() throws CrudException, ConflictingRuleException {
		System.out.println("----------- Compatibility Manager Test / Test remove incompatibilities ## BEGIN -----------");
		
		List<PartType> toRemove = new ArrayList<>();
		toRemove.add(BuilderFactory.xmIncompatibilities.get(0));
		
		/****** trying to remove something that is not present ******/
		assertThrows(CrudException.class, () -> manager.removeIncompatibilities(BuilderFactory.xm, Utils.getIterator(BuilderFactory.eh120Requiremets)));
		
		/****** adding to xm all its incompatible parts ******/
		manager.addIncompatibilities(BuilderFactory.xm, Utils.getIterator(BuilderFactory.xmIncompatibilities));
		
		/****** asserting that all incompatibilities are added ******/
		assertTrue(Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.xm)).containsAll(BuilderFactory.xmIncompatibilities));
		assertTrue(Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.xm)).contains(BuilderFactory.eg100));
		
		/****** removing eg100 from xm' incompatibilities ******/
		manager.removeIncompatibilities(BuilderFactory.xm, Utils.getIterator(toRemove)); //remove eg100
		assertFalse(Utils.convertIteratorToList(manager.getIncompatibleParts(BuilderFactory.xm)).contains(BuilderFactory.eg100));
		
		toRemove.clear();
		toRemove.add(BuilderFactory.xm);
		
		/****** trying to remove itself, robustness test ******/
		assertThrows(CrudException.class, () -> manager.removeIncompatibilities(BuilderFactory.xm, Utils.getIterator(toRemove)));
		
		System.out.println("----------- Compatibility Manager Test / Test remove incompatibilities ## END -----------");
	}
	
	@Test
	@Tag("robustness")
	@DisplayName("remove requirements")
	public void testRemoveRequirements() throws CrudException, ConflictingRuleException {
		System.out.println("----------- Compatibility Manager Test / Test remove requirements ## BEGIN-----------");
		
		List<PartType> toRemove = new ArrayList<>();
		toRemove.add(BuilderFactory.tc120Requiremets.get(0));
		
		/****** trying to remove something that is not present ******/
		assertThrows(CrudException.class, () -> manager.removeRequirements(BuilderFactory.tc120, Utils.getIterator(BuilderFactory.eh120Requiremets)));
		
		/****** adding to tc120 all its required parts ******/
		manager.addRequirements(BuilderFactory.tc120, Utils.getIterator(BuilderFactory.tc120Requiremets));
		
		/****** asserting that all required are added ******/
		assertTrue(Utils.convertIteratorToList(manager.getRequiredParts(BuilderFactory.tc120)).containsAll(BuilderFactory.tc120Requiremets));
		assertTrue(Utils.convertIteratorToList(manager.getRequiredParts(BuilderFactory.tc120)).contains(BuilderFactory.eh120));
		
		/***** removing eh120 from tc120' requirements *****/
		manager.removeRequirements(BuilderFactory.tc120, Utils.getIterator(toRemove)); 
		assertFalse(Utils.convertIteratorToList(manager.getRequiredParts(BuilderFactory.ta5)).contains(BuilderFactory.eg100));
		
		toRemove.clear();
		toRemove.add(BuilderFactory.ta5);
		
		/***** trying to remove itself, robustness test ****/
		assertThrows(CrudException.class, () -> manager.removeRequirements(BuilderFactory.ta5, Utils.getIterator(toRemove)));
		
		System.out.println("----------- Compatibility Manager Test / Test remove requirements ## END-----------");
	}

}
